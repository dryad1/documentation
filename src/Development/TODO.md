## Scheduled Changes/Tasks
- [ ] Forbid use of cbuffers.
- [ ] Change `GraphicsInput` to have a drop implementation and make it be creatable only
    by the gpu api.
- [ ] Implement a proc macro for GpuSerialization.
- [ ] Fix bug in PGA demo. Normals should be transformed by skinning transformation.
- [ ] Add functionality to take screenshots of examples from the terminal.
- [ ] Have `HalfEdgeHandle` and related vertex and face handles use a lifetime to prevent handle invalidation.
- [ ] Implement a threadpool.
- [ ] Parallelize fluid simulation.
- [ ] Implement sweeping surface paper.
- [ ] Implement gaussian process paper.
- [ ] Implement procedural generation of PGA data types.
- [ ] Port missing PGA types to HLSL.
- [ ] Implement mesh equality method.
- [ ] Replace `*i8` for `*u8` on the vulkan bindings wherever necessary.
- [ ] Implement Catmull-Clark subdivision.

## Completed
- [x] Finish topological skeleton algorithm.
- [x] Move all Obj related code to its own crate.
- [x] Add a `SubMeshViewer` object to the `HalfMesh` to facilitate operating on portions of a mesh.
- [x] Replace current Laplacian solver with a good sparse library as soon as available.
- [x] Implement an "EdgeButterflyIterator" that iterates over all the edges contained in the two triangles incident on an edge.
- [x] Modify triangulation interface to specify holes through points. If one wants a given close polygon to be a whole, then an additional point in the interior of the polygon should be passed to the method instead of relying on orientation.

- [x] Fix bug in 03_gaussian_subdivision example, on AMD hardware it seems to cause a segmentation fault when destroying the vulkan instance on termination (no longer occurs, was likely a driver issue).
- [x] Add Steiner point option to triangulations.
- [x] Improve topological joint meshing.
- [x] Add a check for improper winding order For Half Edge initialization.
- [x] Implement smooth topological junction meshing.
- [x] Implement a convex hull algorithm.
- [x] Rewrite half edge iterators and use unsafe a raw pointers once and for all.
- [x] Delete GLSL support code.
- [x] Rewrite shader parsing of pipeline configurations to use attribute list syntax.
- [x] Implement a 2D fluid simulation.
- [x] Add a builder pattern to the shader request objects.
- [x] Optimize PGA type use in skinned animation to
- [x] Implement a rotor class.
- [x] Remove support for linear algebra skinned meshes (use only pga).
- [x] Separate the skeleton and mesh in pga meshes.
- [x] Implement GP for nalgebra vec3's in the PGA crate.

## Cancelled
~~- [ ] Implement implicit euler step for fluid simulation.~~
~~- [ ] Explain the convex hull algorithm for coplanar faces in a writeup.~~