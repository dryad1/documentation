The goal of each tutorial is to simultaneously introduce a theory concept and show how it can be implemented using SemperEngine. A few will have accompanying documents typeset with typst in order to handle mathematical concepts and explanations.

|             Tutorial              | Description |
|:---------------------------------:|:---|
| ![triangle](images/triangle.png ) | [Spinning Triangle](./Documentation/tutorials/SpinningTriangle.md)  <br /> <br /> Simple introduction to SemperEngine, explains how to render a single triangle that spins over time. As well as the basic commands needed to initialize the API. |
