# Math Writeups

These are some math writeups I have made that might be interesting to others:

|             Write up              | Topic Area |
|:---------------------------------:|:---|
| [Finite Differences](https://gitlab.com/dryad1/documentation/-/blob/master/src/math_blog/Finite%20Differences/main.pdf?ref_type=heads) | Numerical Analysis |
| [Galerkin Method](https://gitlab.com/dryad1/documentation/-/blob/master/src/math_blog/Galerkin%20Intuition/main.pdf?ref_type=heads) | Numerical Analysis |
| [Vertex Enumeration](https://gitlab.com/dryad1/documentation/-/blob/master/src/math_blog/VertexEnumeration/vertex_enumeration.pdf?ref_type=heads) | Discrete Mathematics / Geometry  |
| [Polytopology](https://gitlab.com/dryad1/documentation/-/blob/master/src/math_blog/Polytopology/polytopology.pdf?ref_type=heads) | Procedural Geometry |
| [CharrotGregory](https://gitlab.com/dryad1/documentation/-/blob/master/src/math_blog/CharrotGregory/charrot_gregory.pdf?ref_type=heads) | Parametrization / Procedural Geometry |
| [Parametric Polytopology](https://gitlab.com/dryad1/documentation/-/blob/master/src/math_blog/Parametric%20Polytopology/parametric_polytopology.pdf?ref_type=heads) | Procedural Geometry |


