Welcome to the SemperEngine documentation. SemperEngine is designed to be as modular as possible, the design philosophy is "game engine as a library". The primary reason for this project to exist is my personal frustration with engines designed as frameworks. I am used to doing esoteric experimentation. Tools built for particular purpose end up getting in the way instead of helping me.

I am sharing this project in the hopes it helps other people.

There section to this book is [Tutorials](Tutorials), which provides explanations on how to do complex algorithms using the codebase.