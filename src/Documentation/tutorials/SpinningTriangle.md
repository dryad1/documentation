![image](../../images/triangle.png)

The most basic operation one wants to do on a computer in the graphics setting is getting some geometry onto the screen. More commonly, the geometry is going to be a set of triangles, since GPUs are optimized for rasterizing triangles, lines and points.

The first thing we must do is initialize the rendering API:

```rs
 let mut io_context = IoContext::new("Spinning Triangle", (800, 800), true);
 let mut render_context =
     RenderContext::init_rendering(&io_context, VSync::ASYNCHRONOUS);
```

`io_context` Provides an interface to the IO peripherals in the system, things like the screen, the keyboard, the mouse... It is our main way to query for state changes such as window resizing and mouse events. `render_context`on the other hand allows us to query the state of the rendering hardware as well as to issue commands to manipulate the rendering state or sending data to it.

With both contexts initialized we can load some shaders:

```rs
    let spinning_shader = render_context.add_shader(&vec![
        "examples/01_spinning_triangle/spinning.frag.hlsl".to_string(),
        "examples/01_spinning_triangle/spinning.vert.hlsl".to_string(),
    ]);
```

Shaders are special programs that get compiled for the rendering hardware (i.e. the GPU). SemperEngine uses HLSL as the only supported shader language and it expects both the shader stage and `hlsl` as part of the extensions like in the above example. The rendering crate will compile and link the program under the hood and issue a handle that can be used for constructing rendering commands.

Next we will initialize some geometry and serialize it for the GPU:

```rs
    let triangle = [
        Vertex {
            position: Vec2::new(0.0, 0.5),
            color: Vec3::new(0.0, 1.0, 0.0),
        },
        Vertex {
            position: Vec2::new(-0.5, -0.5),
            color: Vec3::new(1.0, 0.0, 0.0),
        },
        Vertex {
            position: Vec2::new(0.5, -0.5),
            color: Vec3::new(0.0, 0.0, 1.0),
        },
    ];

    let gpu_mesh = GraphicsInput::from_gpu_serializable(&triangle, &mut render_context);
```

We define 3 vertices which need to lie in contiguous memory and then invoke `GraphicsInput::from_gpu_serializable` to send the data to the GPU and construct a description of it which can be used for rendering requests. Not that in this case, SemperEngine already implements the `GpuSerializable` trait for many common containers, but you can always implement the trait for any other structure if not available.

At this point we have all the data we need to construct a render request:

```rs
let render_request = RenderRequest::new(spinning_shader.clone())
    .vertex_input(gpu_mesh.clone())
    .build();
```

The only strict requirement to build a `RenderRequest` is a shader handle like the one we built before. Many other options are available, such as the clear color/depth, images to use as inputs into the shader, images to be outputs to render to, etc... For this example all we need is to have the shader and the geometry.

Now we are ready to do some proper rendering:

```rs
    let mut time = 0.00;
    while io_context.poll_io(&mut None)
    {
        time += 0.02;
        let mat = Mat2::new(
            f32::cos(time),
            -f32::sin(time),
            f32::sin(time),
            f32::cos(time),
        );

        render_context.draw(&render_request, &UBO!((mat, 0)));
        render_context.end_frame(&io_context);
    }
```

We start the rendering loop, there will be no debug UI in this example and thus no need to pass anything into the polling function. We will construct a rotation matrix based on the amount of time that has elapsed and finally we render the frame with the commands:


```rs
    render_context.draw(&render_request, &UBO!((mat, 0)));
    render_context.end_frame(&io_context);
```

We can issue any number of draw calls during a frame, once we are done we indicate the frame is over so syncrhonize events between frames. We also make use of the `UBO` macro to pass uniform data from our rust code to the shader. The only requirements are that the memory layout of the HLSL uniform matches that of the Rust definition and that both structs are trivially copiable (i.e. they don't have pointers or indirection in their fields).

Finally when we are done we free all allocated GPU resources:

```rs
render_context.free_input(gpu_mesh);
```

[Source code](https://gitlab.com/Makogan/neverengine/-/blob/master/examples/01_spinning_triangle/)