#import "@preview/algo:0.3.2": algo, i, d, comment, code
#import "../shared/quick_setup.typ": *

#blog_style([

#figure(
  table(
    columns: (1fr), 
    stroke: none,
    image("images/teaser.png", width: 100%), 
  ),
  kind: image,
) <joint>

#setup([Parametric Polytopology], [
  Meshing topological skeletons is a complicated problem. We describe a technique to parametrically mesh topological joints.

  This is a follow up to a prior #link("https://gitlab.com/dryad1/documentation/-/blob/master/src/math_blog/Polytopology/polytopology.pdf?ref_type=heads")[unpublished paper].
])

= Problem Statement

We consider the following setup. We have a topological skeleton, that is a graph (whose edges are *not* necessarily straight), embedded in $RR^3$.

What we want is to generate a mesh surrounding this skeleton, so that it is watertight (unless otherwise specified) and closely follows the directions specified by the graph.

= Duality

== Dual Polytope
#figure(
  table(
    columns: (1fr), 
    stroke: none,
    image("images/straight_line_direction.svg", width: 50%), 
  ),
  caption: [Each edge of a joint generates a dual plane. ],
  kind: image,
) <duality>

The technique is based upon the realization that given a joint, there exists a duality relationship between the edges of the joint and a set of planes, as seen in @duality. This, in turn, means that if we assign an orientation to each plane, we can produce a convex polytope that surrounds the joint. For a more thourough explanation, refer to @camilo_srcmath_blog_2024.

We will refer to the resulting polytope as the dual polytope.

== Dual Stitching

The dual polytope provides a perfect description of the topological relationships between the geometry. Let us assume we have a joint $J$, $k$ edges incident on that joint $e_i, i in [1, k]$, and $k$ generalyzed cylinders $c_i$that sweep along the paths described by the respective $e_i$.

The convex polytope $P_J$ for the joint $J$ is composed of faces, edges and vertices. Each face corresponds to a graph edge $e_i$ and thus to its matching generalized cylinder $c_i$. So a polytope edge descripes the topological relationship between the cylinders associated toits incident faces. More simply, an edge shares two faces $f_i, f_j$, each associated with a cylinder $c_i, c_j$, thus cylinders $c_i, c_j$ must be connected together with a transition surface, as seen in @stitching.

#figure(
  table(
    columns: (1fr), 
    stroke: none,
    image("images/stitching.png", width: 50%), 
  ),
  caption: [If two cylinders correspond to adjacent faces int he polytope, then they must be connected to each other. ],
  kind: image,
) <stitching>

In practical terms, the boundary of each cylinder will be divided into $n$ sections, where $n$ is the valence of its associated face in the dual polytope. Then a parametric surface will be constructed for each pair of cylinders, uniting them.

This will, however, leave holes in the mesh in between the attached cylinders. Each of these regions needs to be filled up by grabbing the boundaries of the surfaces we generated in the prior step and filling them in.  Said differently, each vertex in the dual polytope corresponds to a blending patch whose boundaries need to match the surfaces connecting each cylinder, as seen in @dual_vertex.

#figure(
  table(
    columns: (1fr), 
    stroke: none,
    image("images/hole_region.png", width: 50%), 
  ),
  caption: [ To each vertex in the dual polytope, corresponds a blending patch. ],
  kind: image,
) <dual_vertex>

In our case, we use a Charrot Gregory patch @charrot_pentagonal_1984 to interpolate between the boundaries and match their tangents, closing up the shape smoothly, as shown in @closed.

#figure(
  table(
    columns: (1fr), 
    stroke: none,
    image("images/closed.png", width: 50%), 
  ),
  caption: [ We use a blending patch to close up the holes matching the dual polytope vertices. ],
  kind: image,
) <closed>

= Summary

Thus the technique is summarized as follows:

- Find the dual polytope of the joint.
- Connect together cylinders whose dual faces join at the same edge.
- For each vertex in the dual polytope, get each connecting surface corresponding to that edge, then blend their boundaries together.


#bibliography("bibliography.bib")
])