#import "@preview/algo:0.3.2": algo, i, d, comment, code
#import "../shared/quick_setup.typ": *

#blog_style([

#setup([Charrot Gregory Patch], [
  This explains how to construct a Charrot-Gregory @charrot_pentagonal_1984 patch 
  which is a generalization of a Coons patch @coons_surfaces_1967. It blends
  multiple curves together into a unified, smooth surface,
  and also takes into account bitangent information.
])

// +++
= Motivation

Some times you have a set of curves which are meant to describe the boundary of a surface and you want to find
a surface that fits those boundaries and potentially prescribed tangential information. This algorithm does just that.

#figure(
  grid(
    columns: (1fr, 1fr),
    image("images/triangle.png", width: 80%),
    image("images/square.png", width: 80%),
    image("images/pentagon.png", width: 80%),
    image("images/hexagon.png", width: 80%),
    align: (right, left)
  ),
  caption: [Surfaces obtained by blending three, four, five and six curves, respectively.],
) <blends>

// +++
= Charrot Gregory Patch

== Note
This document heavily deviates from the original paper in terms of notation, despite describing the same process. It also explains it in a more general way to apply to more shapes. Also, all index notation is modular so e.g. $v_(i-1)$ when $i=0$ is the same as $v_(k-1)$

== Inputs

The input to the method will be $k$ curves in $RR^3$ such that they are attached at their endpoints, forming a closed, piecewise smooth curve, and an optional set of prescribed bitangents at each of these curves.

The output will be a surface that blends all such points together is smooth, and, optionally, matches the prescribed bitangents.

== Weights
In order to produce the final surface we will need a set of blending weights and a set of sampling points on the boundaries. 

We start by generating a regular polygon $P$ with $k$ sides, that will be mapped to the output surface. Then assume we have a point $p$ within the interior of the polygon that we wish to map to the surface.

We will first compute the orthogonal distance of $p$ along the edge $i$:

 $ lambda_i (p) = p r o j^perp (p, [v_i, v_(i+1)]) $ 

This will generate $k$ positive scalar weights, as seen in @diagram

To generate the weights we will iterate over every vertex $v_i$ then we will define the function:

$ h_i(p) = product^(k-2)_(i=0) lambda_(i+1) $

i.e. we will take the produt of the orthogonal distances to all edges *not* incident on vertex $v_i$, so in figure @diagram that would correspond to $lambda_(i+1),lambda_(i+2), lambda_(i+3)$ when focusing on vertex $v_i$.

#figure(
  image("images/pentagon_diagram.svg", width: 60%),
  caption: [Orthogonal distances of a point to the edges of the polygon.]
) <diagram>

And then we must normalize the weights, thus:

$ w_i(p) = h_i(p) / (sum^k_(j=0) h_j (p)) = (product^(k-2)_(j=0) lambda_(j+1)) / (sum^k_(j=0) product^(k-2)_(j+2) lambda_j) $

== Sample points 

Now, for each corner of the polygon, we will generate a point to be blended using the above weights. First, we will generate two sampling weights:

$ s_i = lambda_(i-1) / (lambda_(i-1) + lambda_(i+1)) space space space space space space space space space space space t_i = lambda_i / (lambda_i + lambda_(i+2)) $

And with these we will be able to define two points on the curves of the surface.

$ p_i = C_(i-1)(1 - t_i) space space space space space space q_i = C_i (s_i) $

Where $C_i$ is the $i$-th curve in the input and we assume the curve has been aprametriced as $C_i: [0,1] arrow RR^3$.

In case we also want tangent information, then we also produce the tangents:


$ arrow(u_i) = T_(i-1) (1 - t_i) space space space space space space  arrow(v_i) = T_i (s_i) $

== $C_0$ Patch

If all we care about is producing a smooth surface that interpolates the boundary, then we can construct the surface as follows.

First construct a intermediary point:

$ r_i = p_i + q_i - C_i (v_i) = C_(i-1) (1 - t_i) + C_i (s_i) - C_i (0) $

Note that $C_i (0) = C_(i-1) (1)$ by construction, and thus $r_i = p_i$ when $s_i$ is $0$ and $r_i = q_i$ when $t_i$ is $0$.

This allows us to create $k$ distinct points which have the following properties:

- If $p$ was on edge $e_i$ of the polygon, then $r_i$ is exactly on curve $C_i$ of the input curves.

- If $p$ was on the vertex $v_i$ of the polygon, then $r_i$ is exactly on the vertex of $RR^3$ at which $C_(i-1)$ and $C_i$ meet. 

Finally we compute our output surface point $p_o$ as:

$ p_o = sum^k_(i=0) w_i r_i $

== $C_1$ Patch

#figure(
  grid(
    columns: (1fr, 1fr),
    image("images/tangent_blend_2.png", width: 80%),
    image("images/tangent_blend_5.png", width: 80%),
    image("images/tangent_blend_3.png", width: 80%),
    image("images/tangent_blend_4.png", width: 80%),
    align: (right, left)
  ),
  caption: [Surfaces by modifying the prescribed tangents of a hexagonal patch.],
) <tangent_blends>

If we also want to match the prescribed tangential information, then we must involve the tangents. In which case we modify the computation of $r_i$ to be:

$ r_i = &p_i + q_i - v_i\
  &-t_i arrow(v_i) - s_i arrow(u_i) + s_i T_(i-1)(1) +t_i T_i (0)\
  &- t_i s_i B_i $

Where $T_i(0), T_(i-1)(1)$ are the prescribed tangents at the endpoints of the respective curves $C_i, C_(i-1)$ at their shared endpoint. And $B_i$ is a twist condition vector, in practice it can be set to $(arrow(u_i) + arrow(v_i)) / (|| arrow(u_i) + arrow(v_i) ||)$.

= Closing thoughts
This document focused on a very "get it done" approach to describe the method and glosses over any proofs as to why it works. The original paper @charrot_pentagonal_1984 has many interesting derivations which are worth a read. Additionally @salvi_multi-sided_nodate has a set of related papers which may be better suited to certain applications.

#bibliography("ShapeInterpolation.bib")
])