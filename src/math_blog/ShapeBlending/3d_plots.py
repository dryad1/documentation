import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Define the grid
x = np.linspace(-15, 15, 1000)
y = np.linspace(-15, 15, 1000)
x, y = np.meshgrid(x, y)

# Define the function
sin_fun = (np.sin(x) + np.sin(y)) * 0.1

# Create the plot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(x, y, sin_fun, cmap='viridis')
ax.set_zlim(-1, 1)

# Labels and title
# Unlabel the ticks
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])
# Remove the axes rendering
ax.axis('off')

plt.savefig('sin.png', dpi=300, bbox_inches='tight', pad_inches=0)

# Show the plot
plt.show()

saddle_fun = (x * y) * 0.005

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(x, y, saddle_fun, cmap='viridis')
ax.set_zlim(-1, 1)

ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])
# Remove the axes rendering
ax.axis('off')

plt.savefig('saddle.png', dpi=300, bbox_inches='tight', pad_inches=0)

plt.show()

u = np.piecewise(x, [x < -5, x > 5, (x >= -5) & (x <= 5)], [0, 1, lambda x: ((x + 5) / 10)**2 * (3.0 - 2.0 * ((x + 5) / 10))])
v = 1 - u
merge_fun = (u * sin_fun + v * saddle_fun) * 0.5

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(x, y, merge_fun, cmap='viridis')
ax.set_zlim(-1, 1)

ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])
# Remove the axes rendering
ax.axis('off')

plt.savefig('merge.png', dpi=300, bbox_inches='tight', pad_inches=0)

plt.show()