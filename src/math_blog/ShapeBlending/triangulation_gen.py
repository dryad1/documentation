import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Delaunay
from scipy.stats import qmc
import json

# Poisson disk sampling
radius = 0.05  # Adjust as needed to get approximately 20 points
rng = np.random.default_rng(seed=5)
engine = qmc.PoissonDisk(d=2, radius=radius, seed=rng)
points = engine.random(0)

border_samples = 4
s = np.random.uniform(-0.2,0.2,border_samples)

points = np.vstack((points, np.array([[0.3, 0.5]])))
points = np.vstack((points, np.array([[0.6, 0.5]])))

for i in range(border_samples):
    t = i * 0.9 / np.float32(border_samples - 1) + 0.1
    points = np.vstack((points, np.array([[0.01 + s[i], t]])))
    points = np.vstack((points, np.array([[0.99 - s[i], t]])))
    points = np.vstack((points, np.array([[t, 0.99 - s[i]]])))
    points = np.vstack((points, np.array([[t, 0.01 + s[i]]])))


# Convert to numpy array for Delaunay triangulation
points = np.array(points)

# Delaunay triangulation
tri = Delaunay(points)

# Plotting the points and edges
plt.triplot(points[:, 0], points[:, 1], tri.simplices)
plt.plot(points[:, 0], points[:, 1], 'o')
plt.show()

# List of edges
edges = set()
for simplex in tri.simplices:
    for i in range(3):
        edge = tuple(sorted([int(simplex[i]), int(simplex[(i+1) % 3])]))
        edges.add(edge)

edges = list(edges)

# Convert points and edges to list format for JSON serialization
points_list = points.tolist()
edges_list = [list(edge) for edge in edges]

# Create a dictionary to hold the points and edges
data = {
    "points": points_list,
    "edges": edges_list
}

# Output the data to a JSON file
filename = "triangulation.json"
with open(filename, 'w') as f:
    json.dump(data, f, indent=4)

print(f"Data has been written to {filename}")