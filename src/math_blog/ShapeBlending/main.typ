#import "@preview/cetz:0.2.2"
#import "@preview/suiji:0.3.0": *
#import "../shared/quick_setup.typ": *



#blog_style([
#setup([Shape Blending], [])

= Introduction

== Function blending

Fundamentally, functions are infinitely dimensional vectors, and a finitely dimensional vector can be seen as a discrete approximation of a function.

In order to blend two functions $f,g: RR arrow RR$, we will need an additional set of functions $u_i$, which I will call blending masks, with the following properties:

- $0 <= u_i <= 1$
- $sum ^N_(i=1) u_i = 1$ everywhere.
- The domains of $u_i, f, g$ overlap.

The simplest case is that of linear blending, in which we only need two blending functions $u, v$:

$ u(t) = cases(
  t < a quad & 0,
  a <= t <= b quad & (t-a) / (b-a),
  t> b quad & 1,
) 
quad quad
v(t) = cases(
  t < a quad & 0,
  a <= t <= b quad & (b - t) / (b-a),
  t> b quad & 1,
)
$

#set align(center)
#cetz.canvas({
  import cetz.plot
  plot.plot(
    size: (8, 4), 
    axis-style: "school-book",
    x-tick-step: none, 
    y-tick-step: none, 
    x-minor-step: 0.00001,
    x-break: true,
    {
      plot.add(
        domain: (-10, 10), 
        samples: 1000,
        style: (paint:red, thickness: 50pt),
        x => if x < -1 {0} else if x>= -1 and x<=1 {(x+1) / 2} else {1}
      )
      plot.add(
        domain: (-10, 10), 
        samples: 1000,
        x => if x < -1 {1} else if x>= -1 and x<=1 {1 - (x+1) / 2} else {0}
      )
    }
  )
})
#set align(left)

Where $a <= b in RR$ represents the interval over which one function transitions to the other. Given this and a compact domain $[a, b] subset D$ we can blend any two functions $f, g$ defined over $D$. For example we could try $f(x) = e^x, g(x) = cos(x)$

#set align(center)
#cetz.canvas({
  import cetz.plot
  plot.plot(
    size: (8, 4), 
    axis-style: "school-book",
    x-tick-step: none, 
    y-tick-step: none, 
    x-minor-step: 0.00001,
    x-break: true,
    {
      plot.add(
        domain: (-10, 10), 
        samples: 1000,
        x => 
          if x < -1 {calc.exp(x)} 
          else if x>= -1 and x<=1 {
            let t = (x + 1.) / 2.;
            calc.exp(x) * (1 - t) + calc.cos(x) * t
          } 
          else {calc.cos(x)}
      )
    }
  )
})
#set align(left)

This will genereate a continous blend between the two functions, but since it is only $C^1$ continuous, the transition between the functions can become aparent, for example if we sitch $cos$ for $sin$:

#set align(center)
#cetz.canvas({
  import cetz.plot
  plot.plot(
    size: (8, 4), 
    axis-style: "school-book",
    x-tick-step: none, 
    y-tick-step: none, 
    x-minor-step: 0.00001,
    x-break: true,
    {
      plot.add(
        domain: (-10, 10), 
        samples: 1000,
        x => 
          if x < -1 {calc.exp(x)} 
          else if x>= -1 and x<=1 {
            let t = (x + 1.) / 2.;
            calc.exp(x) * (1 - t) + calc.sin(x) * t
          } 
          else {calc.sin(x)}
      )
    }
  )
})
#set align(left)

An alternative blending function is the _smooth step_ function @noauthor_smoothstep_2024 $S$ whose definition on the interval $0, 1$ is $S(x) = 3x^2 -2x^3$. Generalizing it to any interval $[a, b]$ we would get $S(x) = 3((x - a) / (b - a))^2 - 2 ((x - a) / (b - a)) ^ 3$ and thus its matching blending function is just $1 - S(x)$. 

#set align(center)
#cetz.canvas({
  import cetz.plot
  plot.plot(
    size: (8, 4), 
    axis-style: "school-book",
    x-tick-step: none, 
    y-tick-step: none, 
    x-minor-step: 0.00001,
    x-break: true,
    {
      plot.add(
        domain: (-10, 10), 
        samples: 1000,
        style: (paint:red, thickness: 50pt),
        x => if x < -1 {0} else if x>= -1 and x<=1 {  
            let y = (x + 1) / 2;
            let t = y * y * (3.0 - 2.0 * y); t} else {1}
      )
      plot.add(
        domain: (-10, 10), 
        samples: 1000,
        x => if x < -1 {1} else if x>= -1 and x<=1 {
            let y = (x + 1) / 2;
            let t = y * y * (3.0 - 2.0 * y); 1-t} else {0}
      )
    }
  )
})
#set align(left)

Using this function to blend $e^x$ and $sin(x)$ instead would give us the following $C^2$ continous function:

#set align(center)
#cetz.canvas({
  import cetz.plot
  plot.plot(
    size: (8, 4), 
    axis-style: "school-book",
    x-tick-step: none, 
    y-tick-step: none, 
    x-minor-step: 0.00001,
    x-break: true,
    {
      plot.add(
        domain: (-10, 10), 
        samples: 1000,
        style: (dash: "dashed") ,
        x => 
          if x < -1 {calc.exp(x)} 
          else if x>= -1 and x<=1 {
            let y = (x + 1) / 2;
            let t = y * y * (3.0 - 2.0 * y);
            calc.exp(x) * (1. - t) + calc.sin(x) * t
          } 
          else {calc.sin(x)}
      )
    }
  )
})
#set align(left)

Which is much prettier.

== Discrete Functional Analysis<DiscreteFunctionalAnalysis>

Functional analysis tells us that we can decompose functions into basis functions, such as the Fourier basis. Thus we can select a finite numer of such basis elements to approximate our function and do analysis on that. Said otherwise, given a function $f$ we can claim that:

$ f(x) approx sum^N_(i=1) a_i phi_i(x) $

For carefully selected weights $a_i$ and basis functions $phi_i$. But then, this means that we can represent our function $f$ through the vector of its coefficients $[a_i]$. And thus blending two functions in this setting can be defined as grabbing the vectors $arrow(F), arrow(G)$ representing the functions $f, g$, the vectors $arrow(U), arrow(V)$ approximating the blending functions $u, v$ and then just computing:

$ arrow(F) dot.circle arrow(U) + arrow(G) dot.circle arrow(V) $

Where $dot.circle$ represents the component-wise vector product. This gives us a good methodology for function blending in the discrete setting.

== The Fourier Series<FourierSeries>

As we said before, functions are vectors, and thus any function space is a vector space and all vector spaces have a basis. A particularly interesting potential basis is the Fourier basis @noauthor_fourier_2024, which is represented by the formula:

$ s_n = C + sum^N_(i=n) A_n cos(2 pi n/P x) + B_n sin(2 pi n/P x) $

Where $P$ is a real number represeting a period, i.e. for an interval $[a,b]$ we can say that $P = |b-a|$.

Now the Fourier series is not a good basis for all functions, because non-smooth functions, for example, may have non-convergent Fourier series. However, let us assume that the functions we are working with are the smooth functions over the interval $[a, b]$, $C^infinity ([a, b], RR)$. 

Note that any linear combination of our series must be periodic by virtue of the fact that 
all components are periodic. Thus a function $f in C^infinity ([a, b], RR)$ projected onto
the basis $s_n(x)$ will be periodic over $R$. This is important to remember, the fourier 
series is a tool that allows us to periodically extend a function beyond its domain.

The other bit that is important is that the Fourier series form the eigenfunctions 
of the Laplacian. This can be seen from the formula and the boundary conditions:

$ Delta f = (partial ^2 f) / (partial x ^ 2); quad f(a) = c_1; quad f(b) = c_2 $

An eigenfunction for a linear operator $D$ is just a function that obeys $D(f) = lambda f$ for some $lambda in RR \\ {0}$.

Thus we want all functions such that:

$ 
f'' = lambda f <=> & f'' - lambda f = 0 \
 <=>& f = c_1 e^(sqrt(lambda) x) + c_2 e^(-sqrt(lambda) x)\
$

And then if we assume $lambda < 0$ [we get](https://tutorial.math.lamar.edu/classes/de/ComplexRoots.aspx) @noauthor_differential_nodate:

$ f = c_1 cos(|lambda| x) + c_2 sin(|lambda| x) $

But since lambda is an arbitrary number we certainly can define:

$ lambda = -(2 pi n) / p $

Giving us our Fourier series. 

This idea of the eigenfunctions of the Laplacian will be particularly useful later, so it's important to keep it in mind.

= Extending to $RR^2$

==  Motivating Example 
Let us now look at the simpler case of $RR^2$ before moving onto more complicated spaces. As 
motivating cases let us grab the functions:

#figure(
  grid(
    columns: 2,
    gutter: 0mm,
    [#image("sin.png", width: 120%)],
    [#image("saddle.png", width: 90%)],
    [$sin(x) + sin(y)$],
    [$g(x, y) = x$ ] 
  ),
) <planar_graphs>

A very simple way to merge both of these funtions would be to blend them across an axis
aligned subdomain. For example we could define our blending function $u$ to be:

$ u(x, y) = cases(
  x < a quad & 0,
  a <= x <= b quad & (x-a) / (b-a),
  x > b quad & 1,
) $

Just like before, and then $v(x, y) = 1 - u(x, y)$. This would allow us to blend the above
functions as $u dot.op f + v dot.op g$. Yielding this function:

#figure(
  image("merge.png", width: 80%),
) <merged_graph>

This is a nice and smooth transition from one function to the other.

== Generalizing the Blending<GeneralizingtheBlending>

In the above example, we chose a blending domain that was both square and axis aligned. 
However, in general, we would like to handle more complex scenarios, thus we will generalize
our blending operation as follows. 

Consider a compact domain $D subset RR^2$ and a partition of it into three subdomains 
$D_f, D_g, D_t$. These sets must be such that they are themselves compact, continous, and
so that no points in the boundaries of $D_f, D_g$ are adjacent to each other (i.e. the 
sets do not border each other). We will want $u$ to be $1$ on $D_f$, $0$ on $D_g$, and
smoothly transition between both over $D_u$.

To do this, assume we have a point $p in D_u$. Define $p_f$ to be a point on $partial D_f$ 
(the boundary of $D_f$) such that $d(p, p_f)$ is minimzed (here $d$ stands for 
geodesic distance). Let $p_g$ be a similar such point on $D_g$. In cases where there may be multiple 
candidates for $p_f, p_g$ we will pick a pair $(p_f, p_g)$ such that their distance is 
minimized as well.

Putting it all together, this is what it would look like:

#figure(
  image("merging.svg", width: 80%),
) <merged_graph>

The triple $p_f, p, p_g$ forms a poly-line connecting the two boundaries. What we will do 
is assign a number $t$ to $p$ defined by $t = (\|p - p_f\|) / (\|p - p_f\| + \|p - p_g\|)$
the blending factor at $p$ would then be $u(p) = 3t^2 -2t^3$. Which would allow us to blend 
both functions.

==  Discretizing the Domain 

#set align(center)
#cetz.canvas({
    import cetz.draw: *

    let json = json("triangulation.json")
    let points = json.points
    let edges = json.edges

    let mod_points = ()
    for point in points {
      mod_points.push((point.at(0) * 7, point.at(1) * 2.5))
    }

    stroke(blue)
    for edge in edges {
      let p1 = mod_points.at(edge.at(0))
      let p2 = mod_points.at(edge.at(1))

      line(p1, p2, stroke: (thickness: 0.05, paint: blue))
    }

    fill(red)
    stroke(none)
    for point in mod_points {
        circle(point, radius: 0.1)
    }
})
#set align(left)

A complex domain in $$RR^2$$ can be approximated via a triangle mesh like in the above diagram. We evaluate the function at each vertex of this mesh and store the result into a list, i.e. a vector, via an integer indexing of the vertices. Thus if the mesh is described by the pair $(V, E)$ where $V$ is the vertex list and $E$ is the edge list, we generate a new vector $F$ such that $F_i = f(V_i)$, where $V_i$ is the ith vertex.

And in this setting, we can perform the exact same blending we did in the continous case of @GeneralizingtheBlending, just by discretixing it. That is, 
we compute our discretized function vectors $F, G, U, V$ just like in @DiscreteFunctionalAnalysis and once more we blend them via:

$ arrow(F) dot.circle arrow(U) + arrow(G) dot.circle arrow(V) $

= Arbitrary 2-Manifold functions 

The prior sections have given us the tools we need to define our problem in the more complicated case of manifolds with curvature. So now we just need to assemble the pieces.

== Discretization

The very first thing we must do is discretize our domain. A manifold with curvature is too complex a domain to try to do symbolic analysis on it, so let's jump straight into discretizing it.

We will do this by representing our shape $M$ as a triangle mesh, as is standard in computer graphics. So we have a planar graph embedded in $RR^3$ as a pair $(V, E)$. Just like in
the $RR^2$ case, we can define a function $f: M arrow RR$ as a vector $F$, such that 
each scalar value on $F$ represents the evaluation of the function at each vertex, i.e. $F_i = f(V_i)$.

The challenging bit in here is definining the blending functions. A simple method, and the one that I used, is to define the two boundaries of our sets $partial D_f, partial D_g$, which should be two continous chains in the vertices of $M$. Then we just use breath first search to compute the graph distance $d(V_i)$ of each node from $D_f$ to $D_g$, and also noting the maximum distance $m$ found. Then our blending parameter is just:

$ t_i = d(V_i) / m $

And thus our blending weight is $u(V_i) = 3t_i^2 -2t_i^3 $ and as always, $v(V_i) = 1 - u(V_i)$.

Thus our blending can be easily computed like before as:

$ arrow(F) dot.circle arrow(U) + arrow(G) dot.circle arrow(V) $

== Function Extension

Our only remaining problem is what happens if our mesh function is only defined in a small subset $D subset M$ of the mesh we are working with. We want to be able to extend this funciton to the entire manifold in a "periodic" way.

The way we will do this is by copying the periodic extension in $R$ discussed in @FourierSeries. There is a way @vaillant_definition_nodate @taubin_signal_nodate to extend the Laplacian to triangle meshes, and this will generate a Laplacian matrix $L$, which is our discrete operator. If we use the cotan formulation for the weights, our matrix will be symmetric positive definite, and thus will have guaranteed eigenvectors which will also be orthogonal to each other. 

Thus we can just approximate the eigenfunctions of the Laplacian as the eigenvectors ${e_i}$ of $L$. Then for each of these eigenvectors, we can compute a restriction to the domain $D$ in which the function $F$ is defined. We do this by grabbing all and only the rows of each $e_i$ which correspond to the vertices in $D$. That is, if $V_i in D$ then we select row $i$ in $e_i$.

We know that ${e_i}$ is an orthogonal basis for the discretized space of functions over $M$ and thus it's restriction will span the space of functions over $D$. All we need to do is project $F$ onto each $e_i$ via the inner product, that is:

$ a_i = F dot.op e_i $

And thus the extension of $F$ onto $M$ will just be $sum^|V|_(i=1) a_i e_i$

(Notes, the dimension of the eigenvecotrs of V is much higher than F, is this a problem?)

#bibliography("bibliography.bib")
])

"Fourier analysis to signals defined on polyhedral
surfaces of arbitrary topology is based on the observation that the
classical Fourier transform of a signal can be seen as the decomposition of the signal into a linear combination of the eigenvectors of
the Laplacian operator. To extend Fourier analysis to surfaces of
arbitrary topology we only have to define a new operator that takes
the place of the Laplacian"

Off the top of my head: Get the eigenvectors for the entire mesh, select n by largest/smallest eigenvalues (bigger eigenvalue=larger lobe harmonic), solve for a decomposition on the patch (just minimize reconstruction error on the patch) then that gives you the extension.

The Fourier series is the eigenfunctions of the Laplacian for $CC$. Thus for any domain $M$, the eigenfunctions $phi_i$ of the Laplacian yield a basis to extend functions into. The mechanism is rather simple. Let $M_s subset M$ be a compact subset of $M$. Let $f$ be a funtion defined over $M_s$. Then we can compute the restrictions $omega_i$ of the eigenfunctions into the subset. Then we can use the inner product to decompose $f$ into that basis via $angle.l f, omega_i angle.r$, yielding a set of scalar coefficients ${alpha_i} subset RR$. And then, quite nautrally, the extension of $f$ to th entire surface is just $sum^n_(i=1) a_i phi_i$. 

+ What is periodicity on a graph?
+ What is the eigenvector of a function operator.
+ Why is the Fourier basis the eigenvectors of the laplace operator
+ Describe 2D fourier basis in an arbitrary compact domain (the inner product).
+ Try to generalize to 2-manifolds.
