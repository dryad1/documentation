
#import "../shared/quick_setup.typ": *

#blog_style([
#setup([Galerkin Intuition], [])

= Preamble

The foundation to understand the FEM is to see how to define the inner product of two functions. 

Let us start by observing that a function $f:[a, b] subset RR arrow RR$ can be approximated by a finite set of uniform samples along its domain. That is if $x_i$ correspond to $n$ ordered samples of consecutive distance $Delta x = (|b-a|) / n$ along $[a,b]$, then the vector $X = [f(x_i)]$ is a discrete approximation of $f$. Similarly, let $Y$ be a vector rerpesenting a discrete approximation of a function $g$ along the same interval and with the same samples.

A first instinct to define the inner product of this approximation would be to simply take the dot product of these vectors $ X dot Y = sum ^n_(i=1) f(x_i) g(x_i) $ However, note that if the number of samples $i$ increases, then the dot product of these approximations would diverge towards infinity. 

Intutively, we would like for them to converge to a number. So as an approach to try to bound the sequence, let us normalize each term in the sum, so as to weight them by the length of the interval between the samples. That is:

$ angle.l X, Y angle.r := sum ^n_(i=1) f(x_i) g(x_i) Delta x $

Where $angle.l dot, dot angle.r$ represents an inner product. This way, as the number of samples increases, their net contribution to the final sum is weighted down by the increasingly smaller $Delta x$ term. But then this is a familiar formulation, since we know that.

$ lim_(Delta x arrow 0) angle.l X, Y angle.r =  lim_(Delta x arrow 0) sum ^n_(i=1) f(x_i) g(x_i) Delta x = integral_a^b f(x)g(x) d x $

And thus the function inner product $integral_a^b dot dot d x$ for functions quite naturally arises from the familiar finite dimensional inner product. 

You will recall from basic linear algebra that the inner product between two vectors retrieves the norm of the projection of one onto the other. That is, the inner product allows one to extract the component of one lement of a vector space that is colinear witha  different element of a vector space. Let us keep this in mind.

= Explanation

Let $D$ denote a differential operator acting on a function $u: RR arrow RR$. Examples of $D$ are:

$ D(u) = u'' +u + x; D(u) = u' / u x; D(u) = log(x)u' + u $

Assume $D$ is a linear operator. And assume we are given a differential equation of the form:

$ D(u) = f $

The goal of the FEM method is to numerically approximate a solution to the above problem. We start by assuming that there is a set $V = {v_i}$ of basis functions such that 

$ u approx sum^N_(i=1) u_i v_i $

Since $V$ is a basis for the space, then the inner product $integral^b_a u v_i d x$ is extracting the magnitude of the component of $u$ that is colinear with $v$ (because function spaces are vector spaces). Or simply put,
that prior integral grabs the bit of $u$ that aligns with $v$.

Thus for any $v_i$ in the basis of $V$ this must hold:

$ integral^b_a D(u) v_i d x = integral^b_a f v_i d x $

Or said otherwise, the projection of $D(u)$ onto $v_i$ must equal the projection of $f$ onto $v_i$, which makes sense as both functions are equal.


Thus our DE can be approximated as:

$ D(sum^N_(i=1) u_i v_i) = f $

And from the prior statement about projections we must have, for all $v_j$ forming the basis of $V$:

$ integral^b_a D(sum^N_(i=1) u_i v_i) v_j d x = integral^b_a f v_j d x $

By linearity of $D$:

$ integral^b_a sum^N_(i=1) u_i D(v_i) v_j d x &= &integral^b_a f v_j d x \
 <=> sum^N_(i=1) u_i integral^b_a D(v_i) v_j d x &= &integral^b_a f v_j d x $

 But then, we can define a few vectors: 
 
 $ U = [u_i] #h(50pt) b = [integral^b_a f v_j d x] #h(50pt) C_j = [integral^b_a D(v_i) v_j] $ 
 
 and note that 
 
 $ sum^N_(i=1) u_i integral^b_a D(v_i) v_j d x = U dot C_j $.

 Thus we can define a matrix $M = [C_j]$ where its rows are given by each $C_j$, which yields:

 $ sum^N_(i=1) u_i integral^b_a D(v_i) v_j d x &= &integral^b_a f v_j d x <=> M U = b $

 Thus if we summarize what we did. 

We got a linear DE, we discretized the unkown by projecting it onto the basis of a finite vector space. Doing this projection yielded a system of equations, because the same $u$ needs to satisfy all the projected equations simultaneously.

Then leveraged linearity of the DE to re-express each individual projected equation as a dot product, and then we re-expressed the entire system as a classic system of linear equations problem.
])