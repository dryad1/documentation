#import "@preview/algo:0.3.2": algo, i, d, comment, code
#import "../shared/quick_setup.typ": *

#blog_style([

#setup([Vertex Enumeration], [
  I will explain the vertex enumeration problem, as well as an algorithm that can be used to solve it in an asymptotically reasonable time. If you only care about the algorithm jump down to the relevant section,
it has been explained in $RR^3$ only because that is the actual implementation in Demiurge.
])

// +++
= Motivation

Why is this useful? For me, I happen to be working on a few geometry problems related to polytopes. If you like Signed Distance fields, you might recognize half-spaces as one of the quintessential building primitives to make scenes in shadertoy. But they are also _very_ important for logistics an operations research. I encourage you to read on the applications of the Simplex Method to understand why they are useful.

// +++
= Background information

// +++
== Polytopes

First, let us define some background information. A polytope is the generalization of polygons/polyhedrons. In other words, in $d$ dimensions, a polytope is a geomteric figure where each face is "flat" and the boundaries of each face are also "flat".

#figure(
  table(
    columns: (1fr, 1fr, 1fr),
    stroke: none,
    image("images/polygon_square.svg", width: 100%),
    image("images/polygon_weird.svg", width: 100%),
    image("images/polyhedron.svg", width: 80%),
  ),
  caption: [Examples of polytopes.],
  kind: image,
) <polytopes>

So very informally, the intuition is that, in 1D a polytope is a vertex, in 2D it is vertices connected by edges forming a polygon, in 3D it is vertices connected by edges (polygons) connected together as polyhedrons, in 4D it is polyhedrons connected together making a 4-polytope...

A specific kind of polytope is a _convex_ polytope. Which is a polytope where any line connecting 2 points in the polytope is fully inside of it. For example in @polytopes The square and the icosahedron are convex polytopes, but the polygon in the middle is not convex.

// +++
== Planes and Half spaces

You may be familiar with the implicit equation of a plane in 3D:

$ P(x, y, z) = a x + b y + c z + d = 0 $

Let us also remember that in $RR^3$ a plane is defined by a point and a normal. Any point $X$ in the plane satisfies.

$ P(X) = (X - p) dot N = 0 <=>  X dot N - p dot N = 0 $

Expanding out the coordinates:

$ P(X) = X_x N_x + X_y N_y + X_z N_z + (- p dot N) = 0 $

Which is exactly the first equation just different notation. Now, notice that the above is true regardless of the magnitude of $N$. Moreover, any $p$ in the plane works. So let us make two assumptions. First, assume the normal is unit length $norm(N) = 1$. Second, the position vector of $p$ is in the direction of $N$ i.e. $N dot p = p$. We would get:

$ P(X) = X dot N - norm(p) N = 0 <=> X dot N - d N = 0 $

For $d = norm(p)$.

Now let's grab any of these representations. The plane divides the space into two sections. In one section all points return a negative number when plugged into $P$, in the other they return a positive number as shown in @halfspace.

#figure(
  image("images/half_space.svg", width: 50%),
  caption: "A half-space divides space into a positive (green) and negative (red) region."
) <halfspace>

If we have $n$ such half-spaces then we can consider the region of all points which are negative for _both_ of them. In effect we are constructing a convex polytope by cutting through space, one plane at a time like in @intersection.

#figure(
  grid(
    columns: (1fr, 1fr, 1fr),
    rows: (60pt, auto),
    image("images/implicit_polytope_1.svg", width: 50%),
    image("images/implicit_polytope_2.svg", width: 50%),
    image("images/implicit_polytope_3.svg", width: 50%),
    image("images/implicit_polytope_4.svg", width: 50%),
    image("images/implicit_polytope_5.svg", width: 50%),
    image("images/implicit_polytope_6.svg", width: 50%),
  ),
  caption: "A convex polytope is constructed as the intersection of half spaces.",
) <intersection>

More formally, if $H^- subset RR^n$ is the set of all points on the negative side of the halfplane $H$ A convex polytope is thus just $sect_(i=0) H_i^-$ where each $H^-_i$ corresponds to one of its faces. Or similarly put, the convex polytope is the set of all points that are negative evaluated for all $H^-_i$.


// +++
= Problem Explanation

== System of linear inequalities

A set of half spaces is commonly represented as a matrix vector pair $(A, b)$
and the polytope will be given by:

$ { X in RR^n | A X <= b } $

Each row $i$ in this inequality will look like:

$ a_(i 1) x_1 + dots.h + a_(i n) x_n <= b_i $

But note that just like before, we can rewrite this as $N_i dot X <= d_i$ for some
distance $d_i$. That is, each row of $A$ can be assumed to be a normal direction
and each row of $b$ a signed distance from the origin to its projection on the plane.

Thus, just like before, $X$ is just the set of all points which lie on the negative side
of all half-spaces defined by the extended matrix $[A | b]$. This is called the
$cal(H)$-representation of the polytope. What we are looking for is the $cal(V)$-represnetation, an explicit list of all vertices in the polytope.

// +++
== Vertices and Edges

=== Planes and Polytope Geometry
In dimension $d$ a vertex can be characterized as the intersection of $d$ hyper-planes.
For example in $RR^2$ a vertex is the intersection of two lines, and in $RR^3$ it is the
intersection of three planes.

=== Degeneracy
We will say that our matrix $A$ is non-degenerate when there are exactly $d$ planes
that intersect at a given vertex of the polytope. For example, in $RR^3$ each vertex of
the polytope is uniquely defined by the intersection of exactly three planes in the
rows of $[A | b]$. Examples of degenerate and non-degenerate vertices of a polytope
can be seen in @degeneracy.

#figure(
  grid(
    columns: (auto, auto),
    image("images/hexahedron.svg", width: 50%),
    image("images/polyhedron.svg", width: 50%),
  ),
  caption: [The vertices of a cube are non-degenerate, there are exactly 3 planes of
  the polytope that intersect at the edge. On the other hand the vertices of an
  icosahedron are degenerate, at each vertex 5 planes intersect at that point.]
)<degeneracy>

The notion of degeneracy matters because it complicates things a little bit. If
we know the polytope is non-degenerate, then as soon as we find $d$ planes that
intersect at that point we are done looking. However, if there are $d + k$ many planes
intersecting at a given vertex then there are $binom(d+k, d)$ many combinations of plane
all of which will yield the same vertex position. This can become computationally
intractable if we don't take proper measures.

Just like vertices are defined by $d$ planes, the edges of the polytope are defined by
exactly $d-1$ planes. And in particular, we will assume that there is no such thing
as a degenerate edge.

=== True and False Edges and Vertices

Most texts on the subject of polytopes will call the following concept "feasability" but
I prefer to talk about true and false edges and vertices. You will notice from
the two examples in @degeneracy that not any three planes produce a vertex or edge in the
polytope. For example, in the cube, the top, left and bottom faces don't even produce
a vertex at all, and in the icosahedron many faces will produce lines which are not
edges of the polyhedron. A true vertex, for us, will be a vertex on the surface
of the polytope and a false vertex one that is not on the surface. Analogously, a true
edge is on on the surface of the polytope, whereas a false one is one off the surface.

I tried solving this problem in arbtirary dimensions but the math turned out to be ahrder than expected, so I will only talk abbout $RR^3$ here.

- *True Vertex:*

  A true vertex is an ordered tuple $v = (alpha_0, dots, alpha_k)$, with $|v| >= 3$
  such that $alpha_0 > alpha_i$ for $i in [1, k]$, such that the point given by the intersection of all the indexed hyper-planes lies in the
  negative space of all hyper-planes defined by $[A|b]$.

- *True Edge:*

  A true edge is an ordered tuple $e = (alpha_0, dots, alpha_(2))$, such that half the line given by the intersection of all the indexed hyper-planes
  lies in the negative half space of all hyper-planes defined by $[A|b]$.

Assume for a moment that we know that the polytope is non-degenerate. Then if I give you
$3$ indices, corresponding to rows of $[A|b]$, you will have a vertex of the form
$v = (alpha_0, alpha_1, alpha_2)$. In this setting, there is a peculiar but nice property,
it doesn't matter which order I give you the $alpha_i$, it will always be true that
any sub tuple of size $2$ of $v$ is an edge. Formally, $e_i = (alpha_j, (alpha_(j+1))$ for $j
in {0, 1, 2}$, where $e_i$ is an edge of the polytope.

This is actually straightforward to prove. We already assumed that $v$ is a
vertex of the polytope. Since the polytope is non-degenerate, each vertex is uniquely
identified by the indices of the rows of $[A|b]$ that define the planes that intersect at
that point. So if I give you the tuple $e_i$, there must be some row $k in [A|b]$ such
that the intersection of all planes pointed by $e_i$ and the plane pointed by $k$
form a vertex in the polytope (by convexity), so $e_i$ is an edge.

Now, you might be able to see why degeneracy is such an issue, all of the above only holds
if the polytope is non-degenerate. If it is, we have two hurdles. First giving you
$3$ many indices to describe a vertex is incomplete, as there will be missing planes
by definition of degeneracy. More problematic, if I give you all planes that intersect
at the same point it is no longer necessarily true that any permutation of $2$ indices of $v$
produces a true edge.

However, there is a solution. We know that a vertex of the polytope makes a cone, i.e.
a vertex with a lot of lines shooting out of it, like in @poly_cone. Well, this cone is a
manifold. More importantly, it is an oriented manifold. That sounds arcane, but all that
means to us is that it is possible to find a sequence of indices of $v$ such that grabbing two consecutive
indices produces a valid edge. You can skip the proof if you want but I will outline
it here:

#figure(
  image("images/poly_cone.svg", width: 20%),
  caption: [Vertices in a polytope always locally look like a cone.
  The number of faces is equal to the number of planes that intersect at that vertex.]
)<poly_cone>

#box([
*_Theorem_*

Given a set of $k >= 3$ indices into the half-spaces of $[A|b]$, it is always
possible to find an ordering $v = (alpha_0, dots, alpha_k)$ of the indices such that
$e = (alpha_i, alpha_(i+1))$ is a true edge.

*_Proof_*

*base case*:

It is true if $k = 3$ because the half-spaces form a simplicial cone.

*inductive case*:

Assume it is true for $k >= 3$. Assume that $v$ is a valid ordering of indices into
$[A|b]$ that form a true vertex. Let there be a new hyper-plane $H$ such that it intersects all planes pointed by
$v$ into the same point. Then $H$ cuts the existing polytope somewhere. Because we assume
no edge is degenerate, it can remove at most one edge out of the cone defined by $v$.

Let $e = (alpha_i, alpha_(i + 1))$ be the edge that lies on the positive
side of $H$. We know $e$ is unique, because if it wasn't then $[A|b]$ would contain
redundant planes which do not correspond to any faces of the final polytope. This is not allowed. Thus we know by the induction hypothesis that
$e$ is a valid edge.

An edge is formed by the
intersection of $2$ faces (i.e. $2$ 2-facets). Mainly, given $R_i = (alpha_i, dots, alpha_(i + 2))$ the $2$ facets are those produced by grabbing $1$ index in $R_i$.
When $H$ intersects the polytope, it will introduce $2$ new edges $d_1, d_2$ where it intersects the two 2-facets $f_1, f_2$ and all other edges will remain unchanged.

Let $beta$ be the index associated with $H$. We need to find a new ordering of the indices
that contains $beta$, generates $d_i$ and contains all other edges of the polytope. If $beta$
is inserted after the $i$ index in the sequence, we will generate
the sequence:

$ v_n = alpha_0, dots, alpha_(i), beta, alpha_(i + 1) dots alpha_(k-1) $

i.e. we are inserting $beta$ in the middle of the indices from $alpha_i$ to $alpha_(i + 1)$.
We can verify that:

- $e$ can not be generated by a contiguous sequence of 2 integers in $v_n$.
- $e$ we can generate all $d_j$ from contiguous sequences of integers in $v_n$.
- All other edges generated by $v$ can still be generated.

And thus we have proven the induction  hypothesis $qed$])<slicing_theorem>

#figure(
  image("images/cone_clip.svg", width: 40%),
  caption: [Planes can be added one by one, clipping one edge of the polytope.
  Each time a plane is introduced this way, two new edges are generated.]
)<poly_clip>

=== Graph Traversal

#link(<slicing_theorem>, [Theorem 1]) provides us with a really strong result.
There is an implicit graph embedded in the representation of a vertex $v$ in tuple
form. As all edges connected to that vertex can be retrieved by just looking at a sliding
window of indices of constant size. We can leverage that to find all vertices
that are connected to a vertex through an edge by doing a simple linear search.
If we have one of the edges $e$ spawning from a vertex $v$ and we restrict the line
it is at to be only the half-line in the direction of the polytope we can find
the closest intersection of that line with all hyper-planes of $[A|b]$ and get the
neighbour.

#figure(
  image("images/line_search.svg", width: 100%),
  caption: [
    We can search among all planes, which one produces the shortest positive distance along that edge from the active vertex. In this case it would be the green plane.
  ]
)<line_search>

More formally, let $p_v$ be the position vector of the vertex. Let $e$ be an edge
parametrized as $e = {p_v + t d : t >=0 }$ and $d$ be the direction of $e$ which
points along the polytope (i.e. $d dot a_k = -1$ for an appropriate row $k$ of $[A|b]$).

Then given a row $r$ of $[A|b]$ the point $p_n$ along $e$ is given by:

$ p_n = p_v + mu_r d $

Since $p_n$ must, by construction, be a point of the hyper-plane $[a_r|b_r]$ we get that:

$ a_r^T (p_v + mu_r d) = b_r <=> mu_r = (b_r - a_r^T p_v) / (a_r^T d) $

Where $mu_r > 0$ and $mu_r <= mu_j, forall mu_j>0, a_j in A$.

This means that once we have an edge from a vertex, we can find the neighbour
on the polytope which shares that edge. All we need to do is iterate over the rows of
$[A|b]$ until we find which one produces the closest point to the vertex as measured by
the above equation. Much like in @line_search.

This is fantastic! We now have a mechanism that given a vertex $V$ it finds _all_ its
neighbouring vertices. So we can use any graph traversal algorithm, like depth first
search to enumerate all the vertices.

=== Unique Vertex Representation

As mentioned before, if the polytope is degenerate at a given vertex, we run into the
problem that there are as many as $binom(d + k, d)$ ways of computing the position of the
vertex. We got around this problem by simply identifying the vertex with all indices
for all planes that intersect at that vertex's position. However, we still
don't have a unique representation for vertices and this makes it hard to know
if two given index sequences represent the same vertex. Of course, the index set
is unique, so we could just make sure that both sets have the same elements and be done.
The issue is this is an $O(n^2)$ operation and we can do much better.

Given any vertex, first, we will sort its indices by size. We will grab the first $d$
indices and construct a first tentative cone. Then we will use the
#link(<slicing_theorem>, [_Slicing Theorem_]) to reintroduce all other indices into the
sequence one at a time.

Through this procedure we can guarantee all resulting index sequences used to represent
vertices will be always unique and can be compared in $O(n)$ time, and since the cost of
the sort  was $O(n log n)$ we are doing much better here than with naive comparisons,
beter yet we get the ordering we need to generate the edges as well.


// +++
= Algorithm and Implementation

Now we can use everything we just learnt and start making our algorithm! Note that
I will specialize the logic for the case of a polytope in $RR^3$ because that's the
domain that Demiurge cares about, but it should be easy to use the above
mathematical formalism to generalize it to other dimensions.

Some notation I will use for the algorithms:

- $M = [A|b]$ is the extended matrix created by appending $b$ to the columns of $A$.

- $M_i, A_i, b_i, dots$ means grabbing the $i^(t h)$ row of the specified matrix.

- $M_alpha, alpha in NN^d, d > 1$ means constructing the matrix created by concatenating all the rows indexed by the tuple $alpha$.

  For example $M_(i j k)$ is $vec(delim: "[", M_i, M_j, M_k)$.

- For row vectors $v_i$ means grabbing the scalar at intex $i$.

//+++
== Helper functions

Let us define a few, simple, algorithms we will use repeatedly.

First we need to find the intersection point of multiple planes, this can be done in many ways, commonly as the solution to $A_alpha X = b_alpha, |alpha| = 3$, if no solution is possible then we return a flag.

#algo(
  title: "Intersect",
  parameters: ("(i, j, k), M",)
)[
  let x = solution to $M_(i j k)X = b_(i j k)$\
  if no solution, return flag\
  else return x
]

We also need to find the signed distance from a point to a plane:

#algo(
  title: "Signed Distance",
  parameters: ("X, i, M",)
)[
  let $(A_i, b_i) = M_i$\
  return $A_i X - b_i$
]

//+++
== Finding a starting vertex

First, we must initialize our query into the implicit polytope graph by  finding at least
one true vertex. We can do this through the following algorithm. Let $M = [A|b]$:

#algo(
  title: "Vertex Initialization",
  parameters: ("M",)
)[

  let v = (0, 1, 2)\

  let position = intersect($M_0$, $M_1$, $M_2$)\
  let index = 3\
  while intersection is not valid (e.g. parallel planes):#i\
      rotate $v$ to the right by 1\
      $v_0 arrow.l$ index\
      recompute intersection with new indices\
      index += 1#d\

  let index = 3\
  let component = 0\

  while index < row_count(M):#i\
      let d = signed_distance(M_i, position)\
      if d >= 0:#i\
        let next_position = intersect($M_v_0$, $M_v_1$, $M_v_2$)\

        if next_position is invalid, continue\

        component = (component + 1) mod 3\
        i = 0#d#d\

  $v arrow.l$ find_all_planes($v, M$)\
  $v arrow.l$ sort_vertex_planes($v, M$)\
]

The above algorithm basically travels through fake edges of the polytope until it finds a true one. It does so by checking if the current vertex lies on the positive side of a half-space, if it does all edges it is connected to are fake. So it grabs one and moves down to the first half plane that invalidated the vertex through one of its edges.

This moves the active vertex strictly closer to the plytope. We repeat this process until no such plane is found, guaranteeing that we found a vertex in the polytope. The cycling behaviour is to avoid falling into a cycle where we keep picking the same fake edge over and over again.

// +++
== Graph Topology

In order to do a graph traversal, we need a unique representation for vertices that obeys the properties of the theorem we proved in the prior section.

First, we need to find all planes that intersect at a given vertex.

#algo(
  title: "Find Planes at Vertex",
  parameters: ([$alpha$], [$M$],)
)[
  let result = {}\
  let position = intersect($alpha, M$)\

  for i in rows(M):#i\
    let sol = intersect($alpha union i, M$)\
    if sol is not valid, continue\

    if sol $approx$ position:#i\
      result = result $union$ {i}#d#d\

  return sol
]

We also need to sort them so that they form a valid vertex representation that yields all edges.

#algo(
  title: "Sort Vertex Planes",
  parameters: ([$alpha$], [$M = [A | b]$],)
)[
  $alpha$ = sort($alpha$)\
  let result = ${alpha_0, alpha_1, alpha_2}$\
  let testing_direction = average the rows of $(A_alpha)$\

  for pivot in 3 to rows($M$):#i\
    for i in 0 to $|$result$|$:#i\
      let ni = (i + 1) mod $|$result$|$\
      let l = $M_i and M_(n i)$\
      let dir = ($l_(e_(2 3)), l_(e_(3 1)), l_(e_(1 2))$)\
      dir = dir \* -sgn(testing_dir $dot$ dir)\

      if dir $dot A_(p i v o t) >= 0$, break#d\

    insert pivot into result at index $n i$#d\

  return result
]

The gist of this one is that we are testing edges already in the result until we find one which is clipped by the plane pointed by `pivot`. We then insert `pivot` into the sequence so as to take out the invalid edge and add two more edges into the sequence.

Now we can enumerate all unique representations of the neighbours of a vertex:

#algo(
  title: "Enumerate Neighbours",
  parameters: ([$alpha$], [$M = [A | b]$],)
)[
  for i in 0 to $|alpha|$:#i\
    let ni = (i+1) mod $|alpha|$\
    let neighbour = line_search($(i, n i), M$)\

    neighbour $arrow.l$ find_planes_at_vertex(neighbour, $M$)\
    neighbour $arrow.l$ sort_vertex_planes(neighbour, $M$)\
]

// +++
== Vertex Enumeration

And now we can finally do a graph traversal through depth first search:

#algo(
  title: "Enumerate Vertices",
  parameters: ([$M = [A | b]$],)
)[
    let start = vertex_initialization(M)\
    depth_first_search(start, enumerate_neighbours)\
]

Since DFS is a common enough algorithm I won't expand it. Just note that I am assuming it has been implemented in a generic way. It assumes that the DFS implementation only requires a function that enumerates the edges in order to produce the graph walk.

In Demiurge this was accomplished through the use of generics and iterators.

// +++
= Resources

In order to solve this problem I had to scout the internet for a while. The arbitrary-dimensional version of this problem is called the "vertex enumeration problem for the $cal(H)$-representation of convex polytopes". It's suprisingly important for logistics and operations research.

I think the oldest algorithm (by official publication date) for this problem is the double description method by Motzkin. et. al. #cite(<DoubleDescriptionMethod>), I am almost certain the algorithm here is just a specialization of it on $RR^3$ but the way I approached the problem is so different that I am not sure if I am entirely doing the same thing. There is an improved version by Fukuda et. al. #cite(<Doubledescriptionmethodrevisited>) published more recently which is probably what one should read to better understand the method.

There is another method that I also referenced a lot, which you might have heard of, the Simplex Method. I cannot for the life of me find the original manuscript, it might actually still be classified by the US military? But anyhow it is common enough to be all over the web, this book by Karloff has a good description of it #cite(<Karloff1991>). It is normally expressed in terms of "tableaus", which I hate because they are confusing. Moreover it usually assumed that $x >= 0$ which is useless to me and it also includes an objective function, which I don't need nor care for. Worse, it only works on non-degenerate polytopes without further work. However, it does provide the insight I care about, that you can cleverly manipulate a set of indices into a matrix describing the half-spaces to move from one vertex to the next.

I think I have to mention #cite(<FUKUDA19971>) also by Fukuda. It seemed super promising at the beginning, but the algorithms in there were designed for asymptotic analysis and not for actual practical work, and it was a nightmare trying to turn them into practical ones, so I gave up.

A lot of people care about degeneracies it seems, in particular it seems Komei Fukuda and David Avis have spent a lot of effort into making resources for vertex enumeration. I did really enjoy reading their paper on reverse search #cite(<AVIS199621>). This was almost what I wanted, but their abstract definition assumes an upper bound on the degree of degeneracy, which I didn't like. But, it does provide the thing that made it all click for me. All of this is just a graph problem in disguise, and really we are just trying to do a graph walk to enumerate the vertices. The tutorial by Fukuda #cite(<RLS>) was super helful to understand all of this as well.

The final one that had a big impact on this little endeavour of mine was this recently published paper by Mendez et. al. #cite(<ConstructingSkeleton>). It is still very abstract and not super practical if all you want is to implement things. But it was much more tractable in its explanation than most of the others. And the pseudocode, although too high level for my liking, at least provided a nice boiler plate to narrow down what an actual implementation should look like.

It is a little sad that with all this work I did not manage to get things fully working in arbitrary dimensions. It's not impossible, really all you need is to keep track of the edges at each vertex as you find them, which is not the worst thing in the world. But it uses a lot of memory and I wanted to find a compact and elegant representation. Oh well, the $RR^3$ version works well enough for what I need it for.

// +++
= Closing remarks

There is an implementation of all of this in Demiurge, you can check it out if you want. I am, at the moment of writing, still looking for ways to make the algoirthm more elegant and efficient, but for now, this is good enough.

It was a fun little math adventure into the world of linear programming, full of trips while trying to visualize 4 dimensional pyramids, questioning the meaning of a graph and realizing that some problems really only have a handful of resources talking about them.

#bibliography("bibliography.bib")
])