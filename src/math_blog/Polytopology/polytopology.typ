#import "@preview/lovelace:0.2.0": *

#show: setup-lovelace

#import "template.typ": project

#show: project.with(
  title: "Polytopology, polytopes for meshing topological skeletons",
  authors: (
    (name: "Camilo Talero", email: "camilotalero96@hotmail.com"),
  )
  
)

#figure(
  image("images/teaser.png"),
  caption: [Polytopology naturally allows for the procedural creation of many different kinds of meshes from a single input skeleton. From left to right, we show sharp square, screw, circular, and bulbous edges. Each one is produced directly from mathematical rules without human intervention.]
) <fig:teaser>


#show: rest => columns(2, rest)

= Abstract <abstract>
Modeling meshes from a structural graph is a challenging problem that appears in procedural geometry generation, algorithmic botany, and content creation tools. We propose a novel method to address this issue. Our work leverages a duality relationship between the joints of a not-strictly-straight graph embedded in $RR^3$ and a set of half-spaces whose intersection forms a polytope. We describe how to define the polytope from an input joint and how this can be used to produce manifold meshes that contain the skeleton. We show how our method generalizes popular state of the art methods while also producing watertight-manifold meshes in many cases where state of the art fails.

#set heading(numbering: "1.1")

= Introduction <introduction>
Topological skeletons are useful for many applications. l-systems @lindenmayer_mathematical_1968 are used in algorithmic botany to model plants @prusinkiewicz_algorithmic_2004. However, their output is just a graph, and other techniques are needed to produce a surrounding mesh. Content creation tools sometimes allow artists to fabricate meshes by drawing a graph and then generating a surrounding mesh. This is a popular way of creating assets for the movie and video game industries - for example, with 3D modeling tools such as Blender @bishop_skin_2011. Ways in which the associated meshes are produced include: creating disjoint meshes through generalized cylinders; defining implicit structures around the nodes and edges of the skeleton then using constructive solid geometry to combine them, then mesh them with techniques such as Marching Cubes or Dual Contouring @lorensen_marching_1987@ju_dual_2002, sweeping a curve profile along edges of the skeleton and then meshing the joint, such as in Ji et al.’s work @ji_b-mesh_2010. Additionally, @livesu_skeleton-driven_2016 describes a way to generate a hexahedral mesh from tubular sub-components of the mesh for the purposes of generating animation-ready content for physically based simulation. Unlike our method, this work assumes the existence of both the skeleton and a surrounding manifold mesh, and focuses on generating hexahedra in the interior of the shape. Panotopoulou explains a mathematical optimal way to produce a surrounding mesh around a skeleton by merging quad tubular structures to spheres at the joints @panotopoulou_scaffolding_2018. However, unlike our method, it is restricted to quad profiles so it cannot support arbitrary edge geometry. Unlike other methods @baerentzen_smi_2012@ji_b-mesh_2010@panotopoulou_scaffolding_2018, our method is not constrained to specific geometry for the edges, we support quad profiles like many other methods, but we also support any arbitrary parametric cylinder, leading to a wide variety of interesting shapes.

In computer graphics, a topological skeleton, usually refers to a non-straight-line graph embedded in $bb(R)^3$. That is, a collection of vertices $V$, represented as euclidean points, and a set of compact curves $E$ such that the boundaries of each curve correspond to two nodes in $V$. Ideally, the meshes constructed from these skeletons should also meet certain important criteria. They should exhibit manifold topology, that is, each edge in the resulting mesh should be shared by exactly two polygons for an interior edge and one polygon for a boundary one. We also want to avoid local self intersections, for all polygons produced to have a consistent winding order and to support at least two kinds of meshing, quad-based and triangle-based. This is because different applications need either one depending on specific constraints @thibeault_triangles_2019. It would be good for any algorithm solving this problem to support a wide variety of shapes, to allow for as many artistic and engineering uses as possible. Our method meets all the constraints while allowing for many arbitrary curve profiles to produce the edges, as shown in @fig:teaser.

A convex polytope in $bb(R)^n$ is a generalization of convex polyhedra for any euclidean space. Texts @grunbaum_convex_2003@ziegler_lectures_1995 on the topic define two equivalent representations for these polytopes. As the intersection of finitely many half spaces, called the $cal(H)$-representation; or as the convex hull of a set of points, called the $cal(V)$-representation. Convex polytopes in the $cal(H)$-representation are often expressed through a system of linear inequalities. And $cal(V)$-representations can be expressed as graphs with an imbued geometry. All of these are widely studied in the literature. For example, the simplex method @dantzig_simplex_1956, can be seen as a depth first search over the nodes of a highly dimensional polytope and is often used in operations research. Convex polytopes have also been used for collision detection @heckbert_graphics_1994. Algorithms to compute the convex hull, which is equivalent to the $cal(V)$-representation of a convex polytope are commonly used as the basis for many algorithms.

Parametric surfaces, and in particular, sweeping surfaces, are a popular mathematical tool used in computer aided design (CAD). Sweeping surfaces generalize cylinders by sweeping curves along a generalized path such as a B-Spline or Bezier curve @salomon_sweep_2006. This is a flexible representation for surfaces which provides a lot of freedom for artists and engineers who can create many complex shapes with them. However, these shapes are limited in the kinds of geometry they can represent alone. A big problem is that a single parametric surface cannot represent surfaces with genus greater than one, thus many shapes cannot be represented as a single parametric equation. In particular, there currently is no singular clear way on how to stitch together different sweeping surfaces along the arms of a topological joint in a skeleton. For example, if three curves are to meet at the same point. Some approaches exist in certain cases @baerentzen_smi_2012@ji_b-mesh_2010@panotopoulou_scaffolding_2018 but this is an area of active research.

Our method leverages a joint-polytope duality condition, which allows us to stitch generalized cylinders together at each joint in the skeleton. This produces watertight-manifold meshes which can be quad dominant or fully triangular and which exhibit interesting geometric features. Our method allows for a much wider array of shapes than state of the art for the same use cases, while also opening new ones through support of more complex edge geometry.

= Related Work <related-work>
== Skeleton Meshing <skeleton-meshing>
Different authors have worked on approaches to mesh topological skeletons.

BMeshes @ji_b-mesh_2010 work by sweeping a quad along curve edges of a skeleton from root nodes to junctions. Then a convex hull of the resulting points near the joint is taken, closing the shape near the junction. Then post processing operations are applied to the geometry to produce smooth meshes. This creates quad dominant meshes which are suitable for many applications, however, it is limited to quad profiles for the sweeping step and can break in joints where the edges form sharp angles as we will discuss later.

A different approach, SQM, has been proposed @baerentzen_smi_2012. They suggest to add polyhedra to each joint, which they call branch node polyhedra (BNP). These polyhedra have their vertices match with the joint directions and then some of their faces are selected, extruded and connected along the edges. This also produces quality quad dominant meshes. However, their stitching procedure requires that the edges between joints are straight, is also limited to specific tube geometry along edges, and requires a intermediary graph transformation step. In contrast our method works directly in the ambient space without need to transform the skeleton and supports more different edge geometries.

@panotopoulou_scaffolding_2018 also suggest to solve this problem via a sphere-quad cylinder stitching procedure that takes inspiration from @baerentzen_smi_2012, they compute the intersections of quads across the surface of a sphere and use this as a basis for creating the connectivity of the geometry surrounding the joint. Like @baerentzen_smi_2012 they are limited to quad edge profiles.

== Polytopes <polytopes>
Polytopes are a well researched topic in computational geometry.

There exist two important representations of polytopes, as a set of linear inequalities, called the $cal(H)$-representation, or as the convex hull of a set of vertices, called the $cal(V)$-representation.

The $cal(H)$-representation is of the form $A x lt.eq b$ with $A in bb(R)^(m times n)$, where $n$ is the dimension of the polytope, $m$ is the number of faces in the polytope, $x in bold(upright(R))^n$ is a point in space, and $b in bb(R)^m$ is a set of constant values which denote the boundaries of the polytope. An equivalent way to think about the $cal(H)$-representation is as the intersection of half spaces. A half space $cal(H)$ in dimension $n$ is the set of all points that lie on one side of a $n - 1$ subspace, more formally $H = { x : N dot.op x lt.eq b }$ where $N$ is the normal to the subspace and $b$ a signed distance from the origin to its projection on the subspace boundary @grunbaum_convex_2003@ziegler_polytopes_1995.

The $cal(V)$-representation on the other hand can be defined as the convex hull a set of unique points $V$ which form the corners of the polytope @grunbaum_convex_2003@ziegler_polytopes_1995.

== Vertex Enumeration <subsec:vert_enum>
A well known problem related to polytopes is the problem of #emph[vertex enumeration]. It consists on converting an $cal(H)$-representation of a polytope into a $cal(V)$-representation. An extensive body of work @avis_pivoting_1992@avis_reverse_1996@avis_tutorial_2000@motzkin_motzkin_1953@fukuda_double_1996 related to this problem exists. In particular, Avis and Fukuda presented the reverse linear search (RLS) algorithm @avis_reverse_1996@avis_tutorial_2000 to solve the vertex enumeration problem. A short summary of RLS is that it leverages graph-like structures between abstract elements to do a graph search. In the particular case of vertex enumeration, one observes that vertices in a convex polytope are connected by edges in the convex hull. Each vertex is the intersection of the faces that meet at that point. Each edge is the intersection of $n - 1$ faces of the polytope. Thus one can establish a graph like structure for an $cal(H)$-representation of a convex polytope by selecting an appropriate subset of the half spaces that make up the polytope. This will be explained in more detail in @sec:vert_enum.

= Polytopes <polytopes-1>
We rely on the following observations and methods to assemble our algorithm.

== Duality <duality>
We observe that in a joint in a skeleton imbued with some geometry, there exists a duality relationship between each arm connected to that joint and a half space.

For example, let’s assume a straight-line graph. Then we can associate to each arm the direction formed by the joint and the vertex in the skeleton it is connected to, like in @fig:duality_joint. In more general cases, a direction $d$ of unit length can be chosen for each arm. One possible candidate is the direction of the line given by the two nodes of the edge corresponding to the arm, another, if the geometry of the arm is smooth, is the tangent direction of the curve at the joint.

#figure(
  [#image("images/straight_line_direction.svg", height: 15em)],
  caption: [
    Each edge (colored solid edges) connected to a joint defines a direction. Each direction defines an associated half space (dotted lines). Thus for each joint, there exists an associated polytope (in pink) that is the intersection of all such half spaces.
  ],
)
<fig:duality_joint>

Each such direction implicitly defines a set of half spaces through the set of inequalities $d dot.op x lt.eq c$ where $x in bb(R)^n$ and $c$ is an appropriately chosen value. In particular, if a coordinate frame is chosen such that the joint lies at the origin, then $c$ can be thought of the distance from the joint to its projection on the half space boundary. Depending on the choice of $c$, the boundary of the chosen half space will be closer or farther from the joint.

Assume we select a single such half space for each edge connected to a joint. Then the joint as a whole can be associated to a polytope by taking the intersection of all the half spaces associated with each arm. We refer to this intersection as the #emph[dual polytope].

== Polytope Closure <polytope-closure>
We will say a polytope is #emph[closed] if it forms a compact set that is isomorphic to the unit ball of dimension $n$. A polytope is not closed if it fails this condition.

The prior duality condition is guaranteed to define a polytope, however, it does not guarantee that said polytope will be closed. We, nonetheless, will require that for any joint, its associated polytope be closed. Thus we need to device an extension of the duality condition that guarantees closure. We focus on the case of $bb(R)^3$, the following may generalize to other dimensions but we have not explored those spaces in our work.

Given a unit direction $d in bb(R)^3$ there exist two vectors $arrow(u) , arrow(v)$ which form an orthonormal basis for its complement. Thus consider the set $P = { d , - d , arrow(u) , arrow(v) , - arrow(u) , - arrow(v) }$. If we associate the same distance $c$ from the origin along each of the elements of $P$ then the intersection of the resulting half spaces is trivially a cube. Moreover, if we grab any element $w$ of $P$ and rotate it into a new direction $w prime$, as long as the angle between $w$ and $w prime$ is less than 90 degrees, the polytope expressed by $P$ will remain closed.

Given the above we can close a polytope through a simple heuristic driven method, which we explain in more detail in @alg:polytope-closure. The idea is, for each element $d_i$ in a set of directions $D$, construct the set of six elements $P$ as in the prior paragraph. Then for each element $w$ of $P \\ { d_i }$, find the direction $d_j$ that forms the closest angle with ${ d_i }$ if such angle is greater than 90 degrees then we append $w$ to $D$. This procedure is guaranteed to create a closed polytope after a few iterations.

We could have used more sophisticated methods such as Spherical Discrepancy @jones_spherical_2020, however, we find that our simplified scheme works well enough for practical applications and it is simpler to implement.

== Vertex Enumeration <sec:vert_enum>
The problem of vertex enumeration in arbitrarily dimensional spaces has been studied before @motzkin_motzkin_1953@fukuda_double_1996@avis_pivoting_1992@avis_reverse_1996@dantzig_simplex_1956. Of particular interest to us is the work of Avis and Fukuda who presented the RLS algorithm @fukuda_double_1996@avis_pivoting_1992. Inspired by their work we created a simplified version for the case of enumerating vertices from the $cal(H)$-representation of a polytope in $bb(R)^3$. What RLS establishes is that given a set of abstract mathematical objects and a function that yields a local neighborhood relationship for any such object, it is possible to enumerate all elements in the set exactly once through graph search.

In our concrete case, the objects are vertices in space, which are given by the intersections of the boundary planes of three or more half spaces. We will call vertices in the polytope which are contained in exactly three such planes #emph[non-degenerate] and consequently any vertices of the polytope contained more than three planes #emph[degenerate] as in @fig:nondegenerate_corner.

In the case where a vertex $v$ is non-degenerate, then by definition there are three planes who are boundaries of the half spaces that make up the polytope and intersect at $v$. Let us assume a matrix representation for the polytope of the form $A x lt.eq b$. Define then the extended matrix:

$ M = mat(A, b, augment: #1, delim: "[") $

Let $M_(i *)$ denote the $i^(t h)$ row of $M$. Then, by construction, there are three rows in the inequalities, indexed by $alpha_1 , alpha_2 , alpha_3$, such that the planes given by the equations:

$ M_(alpha_i *) dot.op lr((x xor 1)) = 0 $

with $i in { 1 , 2 , 3 } , x in bb(R)^3 , x xor 1 in bb(R)^3 times { 1 }$, form the vertex $v$. In English, we select three half spaces in the polytope such that their boundaries intersect at $v$.

#figure(
  [#image("images/corners.svg")],
  caption: [
    An example of a non-degenerate corner of the polytope (left) and a degenerate one (right).
  ],
)
<fig:nondegenerate_corner>

Thus we can identify a non-degenerate corner of the polytope through a tuple of three elements $lr((alpha_1 , alpha_2 , alpha_3))$, denoting the indices of $M$ that define the three intersecting planes. To account for degenerate cases, we can just extend this definition such that $alpha = lr((alpha_i)) , i in 1 , dots.h.c , k , k gt.eq 3$ denotes the rows of $M$ defining all the planes that intersect at a corner $v$ of the polytope.

We observe that, in the non-degenerate case, the edges connecting a corner of the polytope to its adjacent neighbours can be defined through a simple sliding window. Given non-degenerate vertex $v = lr((alpha_1 , alpha_2 , alpha_3))$ there are three edges that connect it to its adjacent corners, as shown in @fig:nondegenerate_neighbours. These edges are given by the intersection of the planes $lr((alpha_1 , alpha_2)) , lr((alpha_2 , alpha_3)) , lr((alpha_3 , alpha_1))$ respectively.

#figure(
  image("images/labeled_corner.svg", height: 10em),
  caption: [
    Three planes $alpha_1 , alpha_2 , alpha_3$ a vertex $v$ that is their intersection, the edges formed by $alpha_i sect alpha_(i + 1)$ lead to the neighbouring vertices $n_1 , n_2 , n_3$, from $v$.
  ],
)
<fig:nondegenerate_neighbours>

Thus in the non-degenerate case, we satisfy the RLS requirement of having a function that can enumerate all edges spawning at a given node in the graph. We can extend this to the degenerate case by observing that one can go from a non-degenerate case to a degenerate one through a simple insertion process. Assume that $alpha = lr((alpha_i)) , i in 1 , dots.h.c , n , n gt.eq 3$ is a degenerate corner of the polytope with position $p$. Then $beta = lr((alpha_1 , alpha_2 , alpha_3))$ form a simplicial cone whose apex is $p$. By construction, any plane $alpha_i , n gt.eq i > 3$ must intersect any pair of elements of $beta$ at $p$, thus any tuple $gamma = lr((alpha_1 , alpha_2 , alpha_3 , alpha_k)) , 3 < k lt.eq n$ should form a cone with four faces given by the indices and with apex at $p$. Any permutation of $gamma$ denotes the same cone. We want to find a permutation that also allows us to enumerate all edges through a sliding window. We can do this by starting with the simplicial cone given by $alpha$. We enumerate each edge $e_(alpha_i alpha_j) = lr((alpha_i , alpha_j))$ and test if the line $alpha_i sect alpha_j$ lies on the positive side of $alpha_k$. Once we find a line such that $e_(alpha_i alpha_j)$ lies on the positive side of $alpha_k$, we know that $alpha_k$ must intersect the planes $alpha_i , alpha_j$ into valid edges of the polytope. And thus we insert $alpha_k$ into the tuple in between $alpha_i , alpha_j$ creating the sequence $alpha_i , alpha_k , alpha_j$.

Thus in general, given a tuple $alpha = lr((alpha_i))$ defining a corner of the polytope, we can find a permutation $beta$ of $alpha$ such that a sliding window enumerates all edges of the polytope connected to the apex of the cone. This can be achieved by leveraging the prior observation to iteratively insert planes pointed by elements of $alpha$ in the right position so that two consecutive entries of $beta$ are always valid edges, as show in @fig:polytopeinsertion.

#figure(
  image("images/cone_clip.svg", height: 10em),
  caption: [
    The faces of a polytope cone can be inserted in an order where consecutive indices yield a valid edge. This can be achieved by testing which edge lies in the wrong side of a clipping plane.
  ],
)
<fig:polytopeinsertion>

= Method <method>
Using the information of the prior section, we now define the set of algorithms we have devised to solve the problem of meshing a topological skeleton.

== Vertex Enumeration <vertex-enumeration>
Our method requires us to construct a polytope for each joint in the skeleton.

As a first step, for each joint, we collect a set of directions for each arm of the joint. Since the set of directions in the graph may not form a closed polytope, we must first ensure the set of directions is closed through @alg:polytope-closure.


#algorithm(
  caption: [Polytope Closure],
  pseudocode(
    line-numbering: false,
    [*Data:* Topological joint $j$ and a function $E$ that returns an
iterator over all edge directions spawning at $j$. A
scalar $s$ denoting the size of the output polytope.],
    [*Result:* A matrix $D$ which is guaranteed to define a closed
polytope.],
[Let $D$ be a matrix where the rows are given by the set of
directions yielded by $E(j)$.],
[*for each* row $r$ of $D$ *do*], ind,
  [Let $d = D_(r \*)$],
  [Compute a basis $u,v$ for its orthogonal complement.],
  [Let $l=[-d,u,v,-u,-v].$],
  [*for* $w in l$  *do*], ind,
    [Find $w'= "argmax"(w dot arrow(x))$ where $arrow(x)$ is a row of $D$ other than $r$.],
    [*if* the angle between $w$ and $w'$ exceeds a threshold of 90 degrees *then*], ind,
      [append $w$ to $D$], ded,ded,ded,
[Let $B$ be a row vector with as many rows as $D$.],
[*for each* row $r$ of $D$ *do*], ind,
  [Compute $b$ from $s$ and $j$ such that the plane given by $D_(r *) dot x + b = 0$ is $s$ units away from $j$ in the positive direction of $D_(r *)$],
  [Assign $b$ to $B_r$], ded,
[Append $B$ to be the last column $D$.],
[*return* D]
  )
) <alg:polytope-closure>

The second step in our vertex enumeration algorithm is to find a vertex that is guaranteed to be in the polytope. To do so, we start from the assumption that all planes contain a non-empty face of the polytope, thus any plane $P_1$ must contain at least one vertex. Then, since the polytope is convex, the plane $P_2$ which forms the largest angle with $P_1$ must intersect $P_1$ at a valid edge $e$. And thus given any point $p$ in the line defined by $e$, the plane intersecting $e$ such that it has the smallest positive distance along $e$ from $p$ must be a corner. This can be seen more clearly in @alg:polytope-corner.

#algorithm(
  caption: [Find Polytope Corner],
  pseudocode(
    line-numbering: false,
[*Data:* A $m times 4$ matrix $A$ defnining the $cal(H)$-representation of a convex polytope.],
[*Result:* Three indices $i,j,k$ such that the planes defined by the respective rows of $A$ intersect at a valid corner of the polytope.],
[Let $P_1$ be the row $A_1*$],
[Let $P_2$ be the row of $A$ with index $i$ such that the angle between the normals of $P_1$ and $P_2$ is maximal across all rows of $A$.],
[Let $P_3$ be any row $k$ other than $1$ or $j$],
[Let $p$ be point given by $P_1 sect P_2 sect P_3$],
[*for each* row $r$ of $A$ other than 1, $i$ or $k$ *do*], ind,
  [Find the signed distance $d$ from $p$ to the boundary of $A_(r*)$],
  [Store the index of plane yielding the smallest positive $d$ into $j$], ded,
  [Store the index of plane yielding the smallest positive $d$ into $j$]
  )
) <alg:polytope-corner>

Assuming that the matrix $A in bb(R)^(m times 4)$ defines the planes that contain the faces of a closed polytope, we can enumerate the vertices of the polytope through a specialization of RLS as discussed in @sec:vert_enum. We first find a set of planes that is guaranteed to intersect at a valid corner of the polytope. Then from this point we can do a depth first search through the implicit edges given by the intersections of appropriate faces in the cone of the corner. This can be achieved through a simple mechanism: given a corner of the polytope and two faces intersecting at one of its edges, the neighbouring corner is defined by the closest positive intersection among all planes in the polytope. More formally, given the apex $p$ of a corner of the poltype and two faces $F_1 , F_2$ intersecting at a line $e$ such that $p$ is in the line, then there are two orientations for $e$. One of these orientations points in the negative direction of all other faces forming the corner, so without loss of generality assume this orientation. Given a plane $P = n dot.op x + b = 0$ in $A$, the vertex at which it intersects the line $e = p + d t , t in bb(R)$ is given by:

$ n dot.op lr((p + d t)) = - b arrow.l.r.double t = frac(- b - n dot.op p, n dot.op d) $

Thus to find the vertex of the corner connected to $j$ through $e$ it suffices to find the plane $P$ in $A$ such that $P eq.not F_1$, $P eq.not F_2$ and the intersection $P sect F_1 sect F_2$ yields the shortest positive distance $t$ from $p$ among all planes in $A$. This is a simple iterative search which is visualized in @fig:line_search.

#figure(
  image("images/line_search.svg"),
  caption: [
    We can search among all planes, which one produces the shortest positive distance along that edge from the active vertex. In this case it would be the green plane.
  ],
)
<fig:line_search>

Thus if we have a vertex that is guaranteed to be in the polytope, such as the one given by @alg:polytope-corner, and we have the ordered tuple of faces that intersect at that vertex, we can enumerate all its neighbours. We do so by enumerating all pairs of consecutive faces and using the above method to detect one of the planes that intersects at the neighbour along that edge. Then we just do one more iteration over all planes to find the ones that intersect at that vertex and sort them to ensure the sliding window of pairs of elements yields the correct edges for that vertex as well, as expressed by @alg:vert_enumeration.

#algorithm(
  caption: [Vertex Enumeration],
  pseudocode(
    line-numbering: false,
[*Data:* A $m times 4$ matrix $A$ defnining the $cal(H)$-representation of a convex polytope.],
[*Result:* The set of vertices defining the corners of the
polytope],
[Let $p_0 = (alpha_1, alpha_2, alpha_3)$ be the point yielded by @alg:polytope-corner.],
[Find the indices ${r_i}$ of all planes in $A$ such that $A_alpha_1* sect A_(alpha_2*) sect A_(alpha_3*) sect r_i = p_0$.],
[Sort the set ${r_i}$ as described in @subsec:vert_enum into the tuple $v_0$],
[*for each* consecutive pair of elements $(i,j)$ in $v_0$ *do*],ind,
  [Find the half line $e subset A_i* sect A_j*$ whose positive region lies on the surface of the polytope.],
  [Find the plane $k$ yielding the point $q_k = e sect A_k$ on the half line, closest to $p_0$.],
  [Store $q_k$ in the list of neighbours $l$.], ded,
[Repeat the above in a depth first search order for all points and store all unique found vertices into a set $V$.],
[return $V$.]
)
) <alg:vert_enumeration>
Once a corner of the polytope is found an a procedure to enumerate all neighbours of a corner is defined, the problem of vertex enumeration is trivial, as it’s just a simple graph traversal.

With the vertices defined we can just construct the polytope via the convex hull, taking care to sort the faces so that each face that matches an edge in the skeleton appears in the same order in which the edges are enumerated.

== Edge Meshing <edge-meshing>
The next step of our algorithm relies on well known operations in the literature. We must construct a mesh for each edge and connect them to the faces of the polytopes constructed in the prior section. We take care to ensure that our result is manifold and hole free unless otherwise required.

Given an edge, not necessarily straight, connecting two joints, we mesh it via the use of sweeping surfaces. Like @ji_b-mesh_2010 we can sweep a quadrilateral, but unlike them, we are not limited to quads or even convex shapes, any closed planar curve can be supported by our method. Even generalized cylinder curves that involve torsion of complex curves are allowed like in @fig:sweeping_torsion. We only require that the iso curves at both ends of the generalized cylinder surface lie on the faces of the polytopes. That is, the generalized cylinder surface associated with an edge $lr((i , j))$ must end in a closed curve which lies mostly parallel to the planes of the faces of the polytopes for joints $i , j$. In our implementation we sweep our curves along B-Splines and use the tangent vectors at the end points to define the directions for the polytope. In this case the curve will be guaranteed to be orthogonal to the tangent direction and thus we can guarantee it lies parallel to the face of the polytope.

#figure(
  stack(image("images/example2.png", width: 50%), image("images/example1.png", width: 50%), dir: rtl,
),
  caption: [
    Our method supports any kind of generalized cylinder surface, even surfaces with torsion.
  ],
)
<fig:sweeping_torsion>

As shown in @fig:method_summary, In order to guarantee manifold topology, we project all points in the closed curve at the end of the curve. Then, we scale the face of the polytope until it is large enough to contain all projected points. Finally, we use a conforming Delaunay Triangulation algorithm as proposed by Sloan @sloan_fast_1993 to connect these projected points with the vertices of the face and we delete triangles in the interior of the closed curve.

#figure(
  [#image("images/stitching.svg")],
  caption: [
    To stitch edge meshes to the polytopes we (1) project the boundary of the surface onto the plane of the face; (2) scale the face until it contains all points; (3) Form a constrained Delaunay Triangulation; (4) finally, we hollow out the interior triangles.
  ],
)
<fig:method_summary>

This can be described by @alg:polytopestitching and produces a new mesh which is guaranteed to be locally manifold for the edge mesh and polytope mesh we just stitched together. By applying this method to all edge meshes and polytope meshes the result is a guaranteed manifold mesh that completely contains the original skeleton in its interior.

#algorithm(
  caption: [Edge Polytope Stitching],
  pseudocode(
    line-numbering: false,
[*Data:* A set of edge meshes $E$ and a set of polytope meshes $P$],
[*Result:* A new manifold mesh merging $E$ and $P$],
[*for each* edge mesh $M_E$ in $E$ *do*], ind,
  [*for each* polytope mesh $M_P$ in $P$ that should be connected to $M_E$], ind,
    [Get the boundary $B_E$ of $M_E$ that matches one face of $M_P$.],
    [Get the face $F$ of $M_P$ that matches $M_E$.],
    [Project $B_E$ onto the plane containing $F$.],
    [Scale $F$ to fully contain $M_E$.],
    [Triangulate $F union M_E$ via Constrained Delaunay Triangulation.],
    [Delete faces in the interior of $B_E$],
    [Add this topology to the final mesh $M$.],ded,ded,
[return $M$]

)
) <alg:polytopestitching>
== Post Processing <post-processing>
For our work we used a simple post processing step based on simple geometry operations which can be visualized in @fig:postprocessing. First we apply smoothing averaging, that is, each vertex is moved to the average between itself and its neighbours. This procedure is applied a few times based on a user defined iteration count. Next, we use the produced result as a control mesh for Catmull-Clark @catmull_recursively_1978 subdivision in order to produce a higher resolution mesh. Thus, with post processing, our method is summarized in @fig:postprocessing.

#figure(
  [#image("images/pipeline.svg")],
  caption: [
    We take a graph (top left) then create polytopes for each joint (top right), make meshes for the edges, stitch them to the appropriate faces (bottom right) and apply post processing to the resulting manifold mesh (bottom left).
  ],
)
<fig:postprocessing>

It should be noted that this is not required, the meshes can be used "as is" or can be processed by other techniques such as Laplacian smoothing @sorkine_laplacian_2004@herrmann_laplacian-isoparametric_1976.

= Results <results>
We show our method can produce manifold meshes for a wide variety of input topological skeletons, even in cases where other popular methods fail. We show that our method improves the state of the art and can be used for interesting artistic applications.

Our method is better at producing manifold topology, even in cases where other methods fail. We compare our method against the Blender @bishop_skin_2011 implementation of BMeshes @ji_b-mesh_2010. We used the data set by @xu_rignet_2020, we extracted the animation skeletons from each model and processed them to ensure they formed a single connected component. We then processed every skeleton with our method and with BMeshes. We made use of the topological measurements provided by Meshlab @cignoni_meshlab_2008 to compare our results. For each model we expect a watertight mesh with no holes, genus 0, that is two manifold and with a single connected component. @table:speed shows the results for both methods across all criteria. Our method can fail in rare cases where the edge connecting two joints abruptly changes direction near the joint, however it shows an improvement over BMeshes of over forty five times.

#figure(
  [#image("images/errors_2.svg")],
  caption: [
    Our method (top) produces water tight manifold topology in cases where BMeshes (bottom) creates holes. The front side of the mesh is rendered in green while the back faces are rendered red.
  ],
)
<fig:manifold_comparison>

As seen in @fig:manifold_comparison. Since BMeshes take the convex hull of a set of quads, cases where the angles of the arms of the joint are too sharp lead to problems when computing the hull. This operation can be unstable in such cases. This leads to holes and/or non-manifold topology.

Our method does not suffer from these issues because the joint-polytope duality condition and the polytope closure operation are well defined for any arbitrary joint topology. As such, we can guarantee that our method will produce manifold meshes for a larger amount of cases than the state of the art. Unlike other methods, we can also ensure that our meshes fully contain the skeleton.

= Limitations and future work <limitations-and-future-work>
Our algorithm works for many articulated models, however, it struggles to accurately model certain fabricated models such as a chair or any other model that requires large flat regions.

It would be interesting to improve the method by extending and merging the geometric features of the edges into the polytope. This would require a sophisticated blending scheme that smoothly extrapolates the features of each edge mesh into the polytope and then suitably interpolates those features to produce a richer geometry on the surface of the joints that matches human expectation.

Finally, the post processing step proves to be challenging in certain cases. The initial geometry produced by our method does not always lend itself well to common geometric post processing techniques, which limits the range of useful applications. However we are confident this issue can be solved with further research.

= Conclusion <conclusion>
We presented a novel approach to produce a surrounding mesh for a topological skeleton. Our method leverages a duality condition on the joints of a mesh to produce closed polytopes around each joint of a skeleton. Then it uses generalized cylinder surfaces to generate interesting and complex shapes for each edge. Finally, it stitches the polytope meshes together with the tubular meshes to produce a final result which is guaranteed to be manifold. Our method handles cases existing methods cannot while also allowing for a wider variety of possible output models.


#figure(
    align(center)[#table(
        columns: 3,
        align: (col, row) => (left, center, center,).at(col),
        inset: 6pt,
        [Category],
        [Ours],
        [BMeshes],
        [Genus is not zero],
        [4],
        [2205],
        [Holes Present],
        [129],
        [2453],
        [Non-Manifold],
        [0],
        [2008],
        [Disconnected components],
        [0],
        [656],
        [Correct models total],
        [2570],
        [55],
      )],
    caption: [Number of meshes that fail for each method and for each constraint (categories are not disjoint).],
) <table:speed>
= Appendices <appendices>
#block[
  To Manuel Ruivo de Oliveira for his insight on the problem and to Frank Yu, Luis Bolanos, Shih-Yang Su and Ariel Davis for their invaluable help proofreading and improving this work.

]

#show bibliography: set block(below: 0.75em, above: 0pt)
#show bibliography: set par(hanging-indent: 1em)
#show bibliography: set text(size: 0.8em)

#bibliography("bibliography.bib", style: "acmwithname.csl")
