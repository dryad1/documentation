#let project(title: "", authors: (), abstract: "", body) = {
  // Set the document's basic properties.
  set document(author: authors.map(a => a.name), title: title)
  set page(paper: "us-letter", numbering: "1", number-align: center)
  set text(font: "Linux Libertine", lang: "en", size: 0.75em)

  // Title row.
  align(center)[
    #block(text(weight: 700, 1.75em, title))
  ]

  // Author information.
  pad(
    top: 0.5em,
    bottom: 0.5em,
    x: 2em,
    grid(
      columns: (1fr,) * calc.min(3, authors.len()),
      gutter: 1em,
      ..authors.map(author => align(center)[
        *#author.name* \
        #author.email
      ]),
    ),
  )

  // Main body.
  set par(justify: true)
  show figure.caption: set text(size: 0.9em)
  show heading.where(level: 1): it => upper(it)
  show heading: set text(weight: "regular", font: "Linux Biolinum")

  body
}