#let box(text) = block(
    fill: luma(230),
    inset: 8pt,
    radius: 4pt,
    text,
)

#let article_title(title, author, org) = {
  line(length: 100%, stroke: 2pt)
  align(center, text(17pt)[
    *#title*
  ]) 
  align(center)[#text(8pt)[#author\ #org]]
  line(length: 100%, stroke: 2pt)
}

#let setup_outline(intro) = {
  show outline.entry.where(
    level: 0
  ): it => {
    v(13pt, weak: true)
    strong(it)
  }
  [#intro]

  show outline.entry: it => link(it.element.location(),  it.body)
  outline()
}

#let blog_style(body) = {
  set page(
    height: auto,
    width: 390pt
  )
  let in-outline = state("in-outline", false)
  show outline: it => {
    in-outline.update(true)
    it
    in-outline.update(false)
  }

  set heading(numbering: "1.")
  show heading.where(level: 1): title => {
    [*#title*]
    if in-outline.get() {
      
    } else {
      line(length: 100%, stroke: 1.5pt)
    }
  }
  show heading.where(level: 2): title => {
    [*#title*]
    line(length: 100%, stroke: 0.1pt)
  }

  [#body]
}