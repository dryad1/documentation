#import "style.typ": *

#let setup(title, intro) = {
  article_title([#title], [Makogan], [Demiurge])
  setup_outline([#intro])
}