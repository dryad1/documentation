#set math.equation(numbering: "(1)")

#import "../shared/quick_setup.typ": *

#blog_style([

#setup([Intuition for Finite Differences], [])

= Finite Differences

Assume we have a function $f: RR arrow RR$ sampled at regular intevarls with distance $h$.

Consider the point $x_0$ in the domain of $f$.

The taylor expansion of $f$ around $x_0$ gives us:

$ f(x_0) = sum^infinity_(n = 0) (f^((n)) (x_0))/ n! (x - x_0)^n $<taylor>

Let $x_i = x_0 + i h$ for $i in ZZ$

Thus by @taylor we have:

$ 
  f(x_i) = f(x_0 + i h) &= sum^infinity_(n = 0) (f^((n)) (x_0))/ n! (x_i - x_0)^n \
  &= sum^infinity_(n = 0) (f^((n)) (x_0))/ n! (x_0 + i h - x_0)^n \
  &= sum^infinity_(n = 0) (f^((n)) (x_0))/ n! (i h)^n
$

Now, consider finding an order 1 approximation of $f'$. Then we will need at least two samples, and we wish to combine them to find an approximation. Assume that the samples are $f(x_0), f(x_i)$, yielding:

$
 f(x_0) = sum^infinity_(n = 0) (f^((n)) (x_0))/ n! #h(75pt) f(x_1) sum^infinity_(n = 0) (f^((n)) (x_0))/ n! h^n
$

Now, we want to find coefficients $c_1, c_2$ such that the first term of each polynomial is 0, and the second term is 1. i.e. we want $c_1f(x_0) + c_2f(x_1) = 0 f(x_0) + 1 f'(x_0) + cal(O)(h)$

Noting that each $f^(n)$ forms a basis for a vector space, we can express our problem as follows:

$
  c_1 f(x_0) + c_2 f(x_0) &= 0 \
  c_1 0f'(x_0) + c_2 h f'(x_0) &= 1
$

In matrix form:

$
mat(
  delim: "[",
  1, 1;
  0, h
)
mat(
  delim: "[",
  c_1;
  c_2
) = 
mat(
  delim: "[",
  0;
  1
)
$

Which has solutions $c_1 = -1 / h; c_2 = 1 / h$ thus giving $(f(x_0 + h) - f(x_0)) / h$, the classic forward Euler finite difference.

And if we look at the result of the sum:

$ f(x_0 + h) - f(x_0) = \
 (f(x_0) - f(x_0)) + (f'(x_0)h - 0f'(x_0)) + \
 (sum^infinity_(n = 2) (f^((n)) (x_0))/ n! (h )^n - 0)  $

 Thus $(f(x_0 + h) - f(x_0)) / h = f'(x_0) + (cal(O) (h^2)) / h = f'(x_0) + cal(O) (h)$ as desired.

 More generally, we can sample our function $f$ at discrete offsets from a center point $x_0$ and we can generate $n$ such points by going both forward and backwards, for example:

 $ {f(x_0 - h), f(x_0), f(x_0 + h)} #h(50pt) f(x_0), f(x_0 + h), f(x_0 + 2h) $

 Are both 3 point schemes. The taylor expansion at each point will yield polynomials with coefficients of the form $c f^(n)(x_0)$ where $c in RR$. We can thus arrange a set of $n$ samples with offsets $k_i h$, $k_i in ZZ$ in the columns of an infinitely tall matrix:


 $ A = mat(
    delim: "[",
    f(x_0), f(x_0), dots, f(x_0);
    f'(x_0)(k_1 h), f'(x_0)(k_2 h), dots, f'(x_0)(k_n h);
    f''(x_0)(k_1 h)^2, f'(x_0)(k_2 h)^2, dots,  f''(x_0)(k_n h)^2;
    dots.v,dots.v,, dots.v;
    f^(n)(x_0)(k_1 h)^(n-1), f^(n)(x_0)(k_2 h)^(n-1), dots, f^(n)(x_0)(k_n h)^(n-1);
    dots.v, dots.v,,dots.v;
 )  
 $

 i.e. we are stacking the terms of each evaluation of the taylor series at our sample points such that terms of equal differentiation order are on the same row.

 Let $A_n$ be the finite submatrix obtained by grabbing the first $n$ rows of $A$. If we extract just the coefficients and treat $f^(n)$ as the basis elements of a vector space, we get:

 $ A_n = mat(
    delim: "[",
    (k_1 h)^0, (k_2 h)^0, dots, (k_n h)^0;
    (k_1 h)^1, (k_2 h)^1, dots, (k_n h)^1;
    (k_1 h)^2, (k_2 h)^2, dots,  (k_n h)^2;
    dots.v,, dots.v;
    (k_1 h)^(n-1), (k_n h)^(n-1), dots, (k_n h)^(n-1);
 )  
 $

 If we are looking for an approximation of the $i in [0, n-1]$ derivative, then we merely have to create the vector $bold(b) = [delta_(j i)]$, i.e. a vector of 0's everywhere except at the $i$ row. This means that ulimately we are looking for a vector of coefficients $X = [c_i]$ such that:

 $ A_n X = bold(b) $
 
 
 Each sample allows us to reduce the largest coefficient of the error by one, that is, two samples have a maximum error of $cal(O)(h)$, three points will have a maximum error of $cal(O)(h^2)$ and so on.

 Thus for the system produced by $n$ sample points, the worst possible error is of $cal(O)(h^(n-1))$, however, symmetrical samplings aroun $x_0$ can leverage the mirror symmetry of the taylor series and obtain more accurate approximations, such as the central differente $(f(x_0 - h) + f(x_0 + h)) / (2h)$ which has an error of $cal(O)(h^2)$. 

 ])

 